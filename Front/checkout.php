<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/project/ControlPanel/vendor/autoload.php";

use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
echo $appconfig->element('head.php');
echo $appconfig->element('header_and_nav.php');
echo $appconfig->element('nav.php');
?>

<section id="payment">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                   aria-expanded="true" aria-controls="collapseOne">
                                    <h3>1. Personal Information </h3>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                             aria-labelledby="headingOne">
                            <div class="panel-body">
                                <section id="parsonal_chackout">
                                    <div class="parsonal_details">
                                        <!--tab nav-->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#guest" aria-controls="home"
                                                                                      role="tab" data-toggle="tab">
                                                    Order as a guest </a></li>
                                            <li class="nav-item">
                                                <span href="nav-separator"> | </span>
                                            </li>
                                            <li role="presentation"><a href="#signin" aria-controls="profile" role="tab"
                                                                       data-toggle="tab">Sign in </a></li>
                                        </ul>
                                        <!--tab nav-->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="guest">
                                                <form action="">
                                                    <div class="form-grup row">
                                                        <label class="col-md-3">
                                                            Social title
                                                        </label>
                                                        <div class="col-md-6">
                                                            <label class="radio-inline">
                                          <span class="custom-radio">
                                            <input name="id_gender" value="1" checked="" type="radio">
                                          </span>Mr.
                                                            </label>
                                                            <label class="radio-inline">
                                          <span class="custom-radio">
                                            <input name="id_gender" value="2" type="radio">
                                          </span>Mrs.
                                                            </label>
                                                        </div>
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3" for="fname">First Name :</label>
                                                        <input class="col-md-6" type="text" class="form-control"
                                                               id="fname">
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3" for="lname">Last Name :</label>
                                                        <input class="col-md-6" type="text" class="form-control"
                                                               id="lname">
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3" for="email">Email :</label>
                                                        <input class="col-md-6" type="text" class="form-control"
                                                               id="email">
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <p>
                                                        <span class="font-weight-bold">Create an account</span> <span
                                                                class="font-italic">(optional)</span>
                                                        <br>
                                                        <span class="text-muted">And save time on your next order!</span>
                                                    </p>
                                                    <div class="form-group row">
                                                        <label class="col-md-3" for="pass">Passworld :</label>
                                                        <input class="col-md-6" type="password" class="form-control"
                                                               id="pass" required>
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3" for="date">Birthday :</label>
                                                        <input class="col-md-6" type="date" class="form-control"
                                                               id="date">
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-3"></div>
                                                        <span class="custom-checkbox col-md-6">
                                            <input value="1" checked="checked" type="checkbox">
                                            <label>Sign up for our newsletter<br>You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.</label>
                                          </span>
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <a href="">
                                                        <button type="button" class="btn btn-warning"
                                                                style="float: right">continue
                                                        </button>
                                                    </a>
                                                </form>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="signin">
                                                <form action="">
                                                    <div class="form-group row">
                                                        <label class="col-md-3" for="email">Email :</label>
                                                        <input class="col-md-6" type="email" class="form-control"
                                                               id="email">
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3" for="pass">Passworld :</label>
                                                        <input class="col-md-6" type="password" id="myInput">
                                                        <div class="col-md-3"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3"></div>
                                                        <div class="checkbox col-md-6">
                                                            <label>
                                                                <input type="checkbox" onclick="myFunction()">Show
                                                                PassWord
                                                            </label> <br> <br>
                                                            <a href="">Forget Your PassWord ?</a>
                                                        </div>
                                                    </div>
                                                    <br> <br> <br>


                                                    <a href="">
                                                        <button type="button" class="btn btn-warning"
                                                                style="float: right">continue
                                                        </button>
                                                    </a>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h3>2 Addresses </h3>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <section id="adresses">
                                    <form action="">
                                        <div class="form-group row">
                                            <label class="col-md-3" for="fname">First Name :</label>
                                            <input class="col-md-6" type="password" class="form-control" id="fname">
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3" for="lname">Last Name :</label>
                                            <input class="col-md-6" type="password" class="form-control" id="lname">
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3" for="company">Company :</label>
                                            <input class="col-md-6" type="password" class="form-control" id="company">
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3" for="address">Address :</label>
                                            <input class="col-md-6" type="password" class="form-control" id="address">
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3" for="address2">Address Complement :</label>
                                            <input class="col-md-6" type="password" class="form-control" id="address2">
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3" for="city">City :</label>
                                            <input class="col-md-6" type="password" class="form-control" id="city">
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3">State :</label>
                                            <div class="col-md-6">
                                                <select class="form-control">
                                                    <option>Chose Your State</option>
                                                    <option>Bangladesh</option>
                                                    <option>India</option>
                                                    <option>Pakistan</option>
                                                    <option>Napal</option>
                                                    <option>US</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3" for="zip">Zip/Pastal Code :</label>
                                            <input class="col-md-6" type="password" class="form-control" id="zip">
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3">Country :</label>
                                            <div class="col-md-6">
                                                <select class="form-control">
                                                    <option>Chose Your Country</option>
                                                    <option>Bangladesh</option>
                                                    <option>India</option>
                                                    <option>Pakistan</option>
                                                    <option>Napal</option>
                                                    <option>US</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3" for="phone">Phone :</label>
                                            <input class="col-md-6" type="password" class="form-control" id="phone">
                                            <div class="col-md-3"></div>
                                        </div>
                                        <a href="">
                                            <button type="button" class="btn btn-warning" style="float: right">
                                                continue
                                            </button>
                                        </a>
                                    </form>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <h3>3 Shipping Method </h3>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingThree">
                            <div class="panel-body">
                                <section id="sohipping_method">
                                    <table class="table">
                                        <tr>
                                            <td class="col-md-1"><input type="checkbox"></td>
                                            <td class="col-md-3 check-img"><img src="images/product/mobile/m1.png"
                                                                                alt="" class="img-responsive"></td>
                                            <td class="col-md-2"><p>Product Name</p></td>
                                            <td class="col-md-3"><p>Dalivary date</p></td>
                                            <td class="col-md-3"><p>Total Cost</p></td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-1"><input type="checkbox"></td>
                                            <td class="col-md-3 check-img"><img src="images/product/mobile/m6.png"
                                                                                alt="" class="img-responsive"></td>
                                            <td class="col-md-3"><p>Product Name</p></td>
                                            <td class="col-md-3"><p>Dalivary date</p></td>
                                            <td class="col-md-2"><P>Total Cost</P></td>
                                        </tr>
                                    </table>
                                    <a href="">
                                        <button type="button" class="btn btn-warning" style="float: right">continue
                                        </button>
                                    </a>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingfour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                    <h3>4 Payment </h3>
                                </a>
                            </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingfour">
                            <div class="panel-body">
                                <section id="bill_pay">
                                    <div class="col-md-offset-2 col-md-10">
                                        <form action="">
                                            <div class="form-grup row">
                                                <label class="radio-inline">
                                          <span class="custom-radio">
                                            <input name="id_gender" value="1" checked="" type="radio">
                                          </span>Bkash
                                                </label> <br>
                                                <label class="radio-inline">
                                          <span class="custom-radio">
                                            <input name="id_gender" value="2" type="radio">
                                          </span>Rocket
                                                </label> <br>
                                                <label class="radio-inline">
                                          <span class="custom-radio">
                                            <input name="id_gender" value="3" type="radio">
                                          </span>Cash On Delavery
                                                </label> <br>
                                                <div class="col-md-3"></div>
                                            </div>
                                            <a href="">
                                                <button type="button" class="btn btn-warning" style="float: right">
                                                    Order
                                                </button>
                                            </a>
                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <section id="right_side">

                    <div class="sidebar">
                        <table class="table">
                            <tr>
                                <td>
                                    <p class="lfloat">1 item </p>
                                    <p class="rfloat"> BDT. 95.00 tk</p><br> <br>
                                    <p class="lfloat">Sipping</p>
                                    <p class="rfloat">BDT. 0.00tk</p>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                    <p class="lfloat">Total (tax excl.)</p>
                                    <p class="rfloat"> BDT. 102.00 tk.</p> <br> <br>
                                    <p class="lfloat">Taxes</p>
                                    <p class="rfloat">BDT. 0.00tk</p>
                            </tr>
                        </table>
                    </div>
                    <p><i class="glyphicon glyphicon-ok"></i> We provide best quality hand-picked products</p>
                    <p><i class="glyphicon glyphicon-ok"></i> We provide best quality hand-picked products</p>
                    <p><i class="glyphicon glyphicon-ok"></i> We provide best quality hand-picked products</p>
                </section>
            </div>
        </div>
    </div>
</section>


<?php
echo $appconfig->element('sponser.php');
echo $appconfig->element('footer.php');
echo $appconfig->element('js.php');

?>
