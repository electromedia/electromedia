<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/project/ControlPanel/vendor/autoload.php";
session_start();
$_SESSION['sId']=rand(1000,100000);
use rashed\Utility\AppConfig;
use rashed\Utility\Products;

$appconfig = new AppConfig();
$product = new Products();
$products = $product->description();

foreach ($products as $product) {

}


echo $appconfig->element('head.php');
echo $appconfig->element('header_and_nav.php');
echo $appconfig->element('nav.php');
?>


<section id="breadcrumbs_nav">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Phone</a></li>
            <li class="active">Huawei Nava A5</li>
        </ol>
    </div>
</section>

<section id="Product_details">
    <div class="container">
        <div class="row">
            <div class="col-md-6">


                <img style="border:1px solid #e8e8e6;" id="zoom_03"
                     src="http://localhost/project/ControlPanel/Assets/uploads/products/<?= $product['picture'] ?>"
                     data-zoom-image="http://localhost/project/ControlPanel/Assets/uploads/products/<?= $product['picture'] ?>"
                     width="411"/>

                <div id="gallery_01" style="width:500px;float:left; ">

                    <a href="#" class="elevatezoom-gallery active" data-update=""
                       data-image="Assets/images/small/image1.png"
                       data-zoom-image="Assets/images/large/image1.jpg">
                        <img src="Assets/images/small/image1.png" width="100"/></a>

                    <a href="#" class="elevatezoom-gallery"
                       data-image="Assets/images/small/image2.png"
                       data-zoom-image="Assets/images/large/image2.jpg">
                        <img src="Assets/images/small/image2.png" width="100"/></a>

                    <a href="#" class="elevatezoom-gallery"
                       data-image="Assets/images/small/image3.png"
                       data-zoom-image="Assets/images/large/image3.jpg">
                        <img src="Assets/images/small/image3.png" width="100"/>
                    </a>

                    <a href="#" class="elevatezoom-gallery"
                       data-image="Assets/images/small/image4.png"
                       data-zoom-image="Assets/images/large/image4.jpg">
                        <img src="Assets/images/small/image4.png" width="100"/>
                    </a>

                </div>
            </div>
            <div class="col-md-6">
                <div class="img_details">
                    <div class="about_product">
                        <h3><?= $product['title'] ?></h3>
                        <p><?= $product['short_description'] ?></p>
                    </div>
                    <div class="product-reference">
                        <p>Reference</p>
                        <span>XYZ001</span>
                    </div>
                    <br>
                    <div class="product-brand">
                        <p>Brand</p>
                        <span>
                         <a href="#">AbcBrand</a>
                         </span>
                    </div>
                    <br>
                    <div class="variant">
                        <span>Variant</span>
                        <ul>
                            <li>8GB</li>
                            <li>16GB</li>
                            <li>32GB</li>
                            <li>64GB</li>
                            <li>128GB</li>
                        </ul>
                    </div>
                    <br>
                    <div class="product_color">
                        <span class="control-label">Color</span>
                        <span class="color1"></span>
                        <span class="color2"></span>
                    </div>
                    <br>
                    <div class="product-prices">
                        <del>BDT. 750.00 tk.</del>
                        <br>
                        <p>BDT. <?= $product['mrp']?> tk</p>
                    </div>
                    <form method="post" action="Views/Cart/store.php">
                        <input type="text" hidden value="<?php echo $_SESSION['sId']?>" name="guestId">
                        <input type="text" hidden value="<?php echo $product['id']?>" name="product_id">
                        <input type="text" hidden value="<?php echo $product['mrp']?>" name="unit_price">
                        <input type="text" hidden value="<?php echo $product['picture']?>" name="picture">
                        <input type="text" hidden value="<?php echo $product['title']?>" name="product_name">
                        <div class="add_card">
                            <span class="control-label">Quantity</span>
                            <input id="select" class="" step="1" min="0" max="" name="quantity" value="1" title="Qty" size="4"
                                   pattern="[0-9]*" type="number">
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-shopping-cart"></i>
                                Add to cart
                            </button>
                        </div>
                    </form>
                    <br>
                    <div class="social-sharing">
                        <span>Share</span>
                        <ul>
                            <li><i class="fa fa-facebook"></i></li>
                            <li><i class="fa fa-twitter"></i></li>
                            <li><i class="fa fa-google-plus"></i></li>
                            <li><i class="fa fa-pinterest"></i></li>
                        </ul>
                    </div>
                    <br>
                    <div class="bio">
                        <p><span class="glyphicon glyphicon-ok"></span> We provide best quality hand-picked products</p>
                        <p><span class="glyphicon glyphicon-ok"></span>Order before 6 PM to get express delivery </p>
                        <p><span class="glyphicon glyphicon-ok"></span>Easy return policy with no questions asked</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="tab_contents" class="discryption">
    <div class="container">
        <div class="row">
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                              data-toggle="tab">
                            Description
                        </a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                            Data sheet
                        </a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
                            Reviews
                        </a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="tab_text">
                            <h5><?= $product['title'] ?></h5>
                            <p><?= $product['description']?></p>

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <table class="table table-bordered">
                            <tr>
                                <td>Height</td>
                                <td>15 Cms</td>
                            </tr>
                            <tr>
                                <td>Width</td>
                                <td>7 Cms</td>
                            </tr>
                            <tr>
                                <td>Weight</td>
                                <td>170 Gms</td>
                            </tr>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="messages">
                        <div class="comment_details">

                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<?php echo $appconfig->element('sponser.php'); ?>
<?php echo $appconfig->element('footer.php'); ?>



<?php

echo $appconfig->element('js.php');
?>
