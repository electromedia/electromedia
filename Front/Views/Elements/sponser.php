<?php
include_once $_SERVER['DOCUMENT_ROOT']."/project/ControlPanel/vendor/autoload.php";
use rashed\Utility\Sponser;
$result=new Sponser();
$sponsers=$result->frontView();

?>


<section id="demos" class="hidden-xs">
    <div class="container">

        <!--content start-->
        <h2>Our Sponsors</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto eius, eum facilis nihil quaerat
            tenetur.</p>

        <div class="row">
            <div class="col-md-12 col-lg-12">

                <div class="owl-carousel owl-theme">
                    <?php
                        foreach ($sponsers as $sponser):
                    ?>
                    <!--item 1-->
                    <div class="item">

                        <img src="http://localhost/project/ControlPanel/Assets/uploads/sponser/<?= $sponser['picture']?>" class="img-fluid"/>
                    </div>
                    <?php
                        endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>