<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Banner;

$result = new Banner();
$banners = $result->frontView();


?>
<div class="bt-carousel hidden-xs">
    <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php
            $active = 'active';
            for ($i = 0; $i < count($banners); $i++):
                ?>
                <li data-target="#carousel" data-slide-to="0" class="<?= $active ?>"></li>
                <?php
                $active = '';
            endfor;
            ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php
            $active = 'active';
            foreach ($banners as $banner):
                ?>
                <div class="item <?= $active ?>">
                    <img src="http://localhost/project/ControlPanel/Assets/uploads/<?= $banner['picture'] ?>" alt=""
                         class="img-responsive">
                </div>
                <?php
                $active = '';
            endforeach;
            ?>
        </div>

        <!-- Controls -->
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#carousel" data-slide="prev"><i class="fa fa-angle-left"
                                                                               aria-hidden="true"></i></a>
        <a class="carousel-control right" href="#carousel" data-slide="next"><i class="fa fa-angle-right"
                                                                                aria-hidden="true"></i>
        </a>
    </div>
</div>

