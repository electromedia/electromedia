<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/project/ControlPanel/vendor/autoload.php';
$user=$_SESSION['sId'];

use rashed\Utility\Cart;
$cart=new Cart();
$result=$cart->listIndex($user);

$dbh = new PDO("mysql:host=localhost;dbname=electro-media", "root", "");
$query = "SELECT COUNT(*)as id FROM `carts` WHERE `sId`=$user ";
$itemCount = $dbh->query($query);
foreach ($itemCount as $row) {
    $item = $row['id'];
}


?>


<header class="">
    <!--search-->
    <div class="container logo-bar">
        <div class="row header-top hidden-xs hidden-sm">
            <div class="col-md-7 col-md-offset-5">
                <ul>
                    <li class="account">
                        <a href="#"><i class="fa fa-user"></i>My account</a>
                        <div class="signin">
                            <div class="wrapper fadeInDown">
                                <div id="formContent">
                                    <!-- Tabs Titles -->
                                    <h2 class="active text-center"> Sign In </h2>

                                    <!-- Icon -->
                                    <div class="fadeIn first">
                                        <img src="Assets/images/logo/electromedia.png" id="icon" alt="User Icon"/>
                                    </div>

                                    <!-- Login Form -->
                                    <form>
                                        <input type="text" id="login" class="fadeIn second" name="login"
                                               placeholder="login">
                                        <input type="text" id="password" class="fadeIn third" name="login"
                                               placeholder="password">
                                        <input type="submit" class="fadeIn fourth" value="Log In">
                                    </form>

                                    <!-- Remind Passowrd -->
                                    <div id="formFooter">
                                        <a class="underlineHover" href="#">Forgot Password?</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="#"><i class="fa fa-heart"></i>Wishlist</a></li>
                    <li><a href="http://localhost/project/Front/shoppingcart.php"><i class="fa fa-shopping-bag"></i>My
                            Cart</a></li>
                    <li><a href="http://localhost/project/Front/checkout.php"><i class="fa fa-check"></i>Check Out</a>
                    </li>
                </ul>


            </div>
        </div>
        <div class="col-md-3 logo-name text-center hidden-xs hidden-sm">
            <a href="index.php"><img src="Assets/images/logo/logo.png" alt="" class="img-responsive"/></a>
        </div>

        <!--search start-->
        <div class="col-md-6 search col-sm-6">
            <div id="custom-search-input">
                <div class="input-group col-md-12 col-xs-12">
                    <form action="" method="post">
                        <input type="text" class="form-control input-lg" name="searchKeyword"
                               placeholder="Electronic Media......." style="width: 435px"/>
                        <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                    </form>
                </div>
            </div>
        </div>


        <div class="col-md-3 shopping-cart col-xs-12 col-sm-6 text-center">
            <div class="icon-round">

                <div class="cart-item">
                    <div class="cart-mail"><a href="shoppingcart.php"><i class="fa fa-shopping-cart"></i><span>
                                <?php
                                if ($item) {
                                    echo $item;

                                } else {
                                    echo 0;
                                }
                                ?>
                            </span></a>
                    </div>
                    <p><a href="shoppingcart.php">My cart</a></p>

                    <div class="cart-item-hover">
                        <?php
                        if (count($result) > 0) {
                            foreach ($result as $cart):
                                ?>
                                <div class="cart-item-list">
                                    <img src="http://localhost/project/ControlPanel/Assets/uploads/products/<?= $cart['picture'] ?>"
                                         alt="" class="img-responsive"/>
                                    <a href="#"><h3><?= $cart['product_title'] ?></a>
                                    <b><a href="Views/Cart/delete.php?id=<?= $cart['id']?>">X</a></b>
                                    <p>Price : <?= $cart['unite_price'] ?></p>
                                </div>
                            <?php
                            endforeach;
                        }
                        else {


                            ?>
                            <tr>
                                <td colspan="5">You have no product on your cart.</td>
                            </tr>
                            <?php
                        }

                        ?>
                        <div class="border"></div>
                        <div class="cart-total">
                            <div class="clearfix"></div>
                            <a href="shoppingcart.php" class="cart-view">View all</a>
                            <a href="checkout.php" class="cart-checkout">Check out</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="clearfix"></div>
    </div>
</header>