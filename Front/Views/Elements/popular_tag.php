<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
use rashed\Utility\PopularTags;

$tags=new PopularTags();
$result=$tags->frontView();
?>


<h3 class="widget-title side-title">popular tags</h3>
<div class="tags-box">
    <?php
        foreach ($result as $tag):
    ?>
    <a href="<?= $tag['link']?>" title="<?= $tag['name']?>"><?= $tag['name']?></a>
    <?php
        endforeach;
    ?>
</div>