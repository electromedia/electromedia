<div class="wsmenucontainer clearfix">
    <div id="overlapblackbg"></div>
    <div class="wsmobileheader clearfix"><a id="wsnavtoggle" class="animated-arrow"><span></span></a> <a
            class="smallogo"><img src="Assets/images/logo/logo.png" alt=""/></a> <a class="callusicon" href="tel:123456789"><span
                class="fa fa-phone"></span></a>
    </div>
    <div class="headerfull">
        <!--Main Menu HTML Code-->
        <div class="wsmain">
            <nav class="wsmenu clearfix">
                <ul class="mobile-sub wsmenu-list">
                    <li>
                        <a href="index.php" class="navtext"><span>Home</span></a>
                    </li>
                    <li><a href="product.php" class="navtext"><span>Mobile & Gadgets</span></a>
                        <div class="wsshoptabing wtsdepartmentmenu clearfix">
                            <div class="wsshopwp clearfix">
                                <ul class="wstabitem clearfix">
                                    <li class="wsshoplink-active"><a href="#"><i class="fa fa-laptop"></i>Laptop &amp; Accessories</a>
                                        <div class="wstitemright clearfix wstitemrightactive">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-12 clearfix">
                                                        <div class="wstheading clearfix">Hp</div>
                                                        <ul class="wstliststy01 clearfix">
                                                            <li><a href="#">Asus</a></li>
                                                            <li><a href="#">Lenovo<span
                                                                        class="wstmenutag greentag">New</span></a>
                                                            </li>
                                                            <li><a href="#">Dell</a></li>
                                                            <li><a href="#">Samsung<span
                                                                        class="wstmenutag bluetag">Trending</span></a>
                                                            </li>
                                                            <li><a href="#">Leggings</a></li>
                                                            <li><a href="#">Swimsuits &amp; Cover Ups</a></li>
                                                            <li><a href="#">Lingerie, Sleep &amp; Lounge</a></li>
                                                            <li><a href="#">Inner &amp; Nightwear</a> <span
                                                                    class="wstmenutag redtag">Sale</span></li>
                                                            <li><a href="#">Jumpsuits, Rompers </a></li>
                                                            <li><a href="#">Coats, Jackets &amp; Vests</a></li>
                                                            <li><a href="#">Suiting &amp; Blazers </a></li>
                                                            <li><a href="#">Socks &amp; Hosiery</a></li>
                                                        </ul>
                                                        <div class="wstheading clearfix">Handbags & Wallets</div>
                                                        <ul class="wstliststy01 clearfix">
                                                            <li><a href="#">Clutches</a></li>
                                                            <li><a href="#">Cross-Body Bags</a></li>
                                                            <li><a href="#">Evening Bags</a></li>
                                                            <li><a href="#">Shoulder Bags</a> <span
                                                                    class="wstmenutag orangetag">Hot</span></li>
                                                            <li><a href="#">Top-Handle Bags</a></li>
                                                            <li><a href="#">Wristlets</a></li>
                                                        </ul>
                                                        <div class="wstheading clearfix">Accessories</div>
                                                        <ul class="wstliststy01 clearfix">
                                                            <li><a href="#">Handbag Accessories</a></li>
                                                            <li><a href="#">Sunglasses Accessories</a></li>
                                                            <li><a href="#">Eyewear Accessories</a></li>
                                                            <li><a href="#">Scarves & Wraps</a></li>
                                                            <li><a href="#">Gloves & Mittens</a></li>
                                                            <li><a href="#">Hats & Caps</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 clearfix">
                                                        <div class="wstbootslider clearfix">
                                                            <div id="demo" class="carousel slide" data-ride="carousel">

                                                                <!-- The slideshow -->
                                                                <div class="carousel-inner">
                                                                    <div class="carousel-item active"><img
                                                                            src="Assets/images/product/computer/c3.jpg" alt=""/>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-mobile"></i> Mobile &amp; Accessories</a>
                                        <div class="wstitemright clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-8 col-md-12 clearfix">
                                                        <div class="wstheading clearfix">Men's Clothing</div>
                                                        <ul class="wstliststy01 clearfix">
                                                            <li><a href="#">Shirts <span
                                                                        class="wstmenutag greentag">New</span></a>
                                                            </li>
                                                            <li><a href="#">Fashion Hoodies & Sweatshirts </a></li>
                                                            <li><a href="#">Pants &amp; Trousers</a></li>
                                                            <li><a href="#">Capris and Shorts </a></li>
                                                            <li><a href="#">Swim</a></li>
                                                            <li><a href="#">Suits & Sport Coats</a></li>
                                                            <li><a href="#">Underwear</a></li>
                                                            <li><a href="#">Socks</a></li>
                                                            <li><a href="#">Sleep & Lounge</a></li>
                                                            <li><a href="#">T-Shirts & Tanks <span
                                                                        class="wstmenutag redtag">20% off Sale</span></a>
                                                            </li>
                                                            <li><a href="#">Active</a></li>
                                                            <li><a href="#">Sport Coats <span
                                                                        class="wstmenutag bluetag">Trending</span></a>
                                                            </li>
                                                        </ul>
                                                        <div class="wstheading clearfix">Shoes & Wallets</div>
                                                        <ul class="wstliststy01 clearfix">
                                                            <li><a href="#">Athletic</a></li>
                                                            <li><a href="#">Boots</a> <span
                                                                    class="wstmenutag orangetag">Exclusive</span>
                                                            </li>
                                                            <li><a href="#">Fashion Sneakers</a></li>
                                                            <li><a href="#">Loafers & Slip-Ons</a></li>
                                                            <li><a href="#">Mules & Clogs</a></li>
                                                            <li><a href="#">Outdoor</a></li>
                                                            <li><a href="#">Oxfords</a></li>
                                                            <li><a href="#">Sandals</a></li>
                                                            <li><a href="#">Slippers</a></li>
                                                        </ul>
                                                        <div class="wstheading clearfix">Accessories</div>
                                                        <ul class="wstliststy01 clearfix">
                                                            <li><a href="#">Belts</a></li>
                                                            <li><a href="#">Suspenders</a></li>
                                                            <li><a href="#">Eyewear Accessories</a></li>
                                                            <li><a href="#">Neckties</a></li>
                                                            <li><a href="#">Bow Ties & Cummerbunds</a></li>
                                                            <li><a href="#">Collar Stays</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 clearfix"><a href="#"
                                                                                                class="wstmegamenucolr"><img
                                                                src="Assets/images/phone.png" alt=""></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-play-circle"></i> Movies, Music &amp; Games</a>
                                        <div class="wstitemright clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12 clearfix">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Latest Movies</li>
                                                            <li><a href="#">Action & Adventure <span
                                                                        class="wstmenutag greentag">New</span></a>
                                                            </li>
                                                            <li><a href="#">Bollywood </a></li>
                                                            <li><a href="#">Documentary</a></li>
                                                            <li><a href="#">Educational</a></li>
                                                            <li><a href="#">Exercise & Fitness </a></li>
                                                            <li><a href="#">Faith & Spirituality</a></li>
                                                            <li><a href="#">Fantasy</a></li>
                                                            <li><a href="#">Romance</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 clearfix">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Newest Games</li>
                                                            <li><a href="#">PlayStation 4 </a></li>
                                                            <li><a href="#">Xbox One </a> <span
                                                                    class="wstmenutag orangetag">Most Viewed</span>
                                                            </li>
                                                            <li><a href="#">Nintendo DS</a></li>
                                                            <li><a href="#">PlayStation Vita </a></li>
                                                            <li><a href="#">Retro Gaming</a></li>
                                                            <li><a href="#">Digital Games</a></li>
                                                            <li><a href="#">Microconsoles</a></li>
                                                            <li><a href="#">Kids & Family </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 clearfix">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Popular Music Genre</li>
                                                            <li><a href="#">Alternative & Indie Rock</a></li>
                                                            <li><a href="#">Broadway & Vocalists</a></li>
                                                            <li><a href="#">Children's Music</a></li>
                                                            <li><a href="#">Christian <span class="wstmenutag bluetag">50% off</span></a>
                                                            </li>
                                                            <li><a href="#">Classic Rock</a></li>
                                                            <li><a href="#">Comedy & Miscellaneous </a></li>
                                                            <li><a href="#">Country</a></li>
                                                            <li><a href="#">Dance & Electronic</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 clearfix">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Popular Music Genre</li>
                                                            <li><a href="#">Alternative & Indie Rock</a></li>
                                                            <li><a href="#">Broadway & Vocalists</a></li>
                                                            <li><a href="#">Children's Music <span
                                                                        class="wstmenutag redtag">Discounted</span></a></a>
                                                            </li>
                                                            <li><a href="#">Classical</a></li>
                                                            <li><a href="#">Classic Rock</a></li>
                                                            <li><a href="#">Comedy & Miscellaneous</a></li>
                                                            <li><a href="#">Country</a></li>
                                                            <li><a href="#">Dance & Electronic</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-6 wstadsize01 clearfix"><a href="#"><img
                                                                src="Assets/images/ad-size01.jpg" alt=""></a></div>
                                                    <div class="col-lg-6 wstadsize02 clearfix"><a href="#"><img
                                                                src="Assets/images/ad-size02.jpg" alt=""></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-television"></i>Tv &amp; Monitor</a>
                                        <div class="wstitemright clearfix kitchenmenuimg">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Home Appliances</li>
                                                            <li><a href="#">Air Conditioners <span
                                                                        class="wstmenutag greentag">New</span></a>
                                                            </li>
                                                            <li><a href="#">Air Coolers </a></li>
                                                            <li><a href="#">Fans</a></li>
                                                            <li><a href="#">Microwaves</a></li>
                                                            <li><a href="#">Refrigerators</a></li>
                                                            <li><a href="#">Washing Machines </a></li>
                                                            <li><a href="#">Jars & Containers </a></li>
                                                            <li><a href="#">LED & CFL bulbs </a></li>
                                                            <li><a href="#">Drying Racks </a></li>
                                                            <li><a href="#">Laundry Baskets</a> <span
                                                                    class="wstmenutag orangetag">New</span></li>
                                                            <li><a href="#">Washing Machines </a></li>
                                                            <li><a href="#">Bedsheets </a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Kitchen Appliances</li>
                                                            <li><a href="#">Air Fryers </a></li>
                                                            <li><a href="#">Espresso Machines</a></li>
                                                            <li><a href="#">Food Processors</a> <span
                                                                    class="wstmenutag bluetag">Popular</span></li>
                                                            <li><a href="#">Hand Blenders</a></li>
                                                            <li><a href="#">Induction Cooktops</a></li>
                                                            <li><a href="#">Microwave Ovens</a></li>
                                                            <li><a href="#">Mixers & Grinders</a></li>
                                                            <li><a href="#">Rice Cookers</a></li>
                                                            <li><a href="#">Stand Mixers</a></li>
                                                            <li><a href="#">Sandwich Makers</a></li>
                                                            <li><a href="#">Tandoor & Grills</a></li>
                                                            <li><a href="#">Toasters</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-television"></i>Electronics Appliances</a>
                                        <div class="wstitemright clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li><img src="Assets/images/ele-menu-img01.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix">TV & Audio</li>
                                                            <li><a href="#">4K Ultra HD TVs </a></li>
                                                            <li><a href="#">LED & LCD TVs</a></li>
                                                            <li><a href="#">OLED TVs</a> <span
                                                                    class="wstmenutag bluetag">Popular</span></li>
                                                            <li><a href="#">Plasma TVs</a></li>
                                                            <li><a href="#">Smart TVs</a></li>
                                                            <li><a href="#">Home Theater</a></li>
                                                            <li><a href="#">Wireless & streaming</a></li>
                                                            <li><a href="#">Stereo System</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li><img src="Assets/images/ele-menu-img02.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix">Camera, Photo & Video</li>
                                                            <li><a href="#">Accessories <span
                                                                        class="wstcount">(1145)</span></a></li>
                                                            <li><a href="#">Bags & Cases <span
                                                                        class="wstcount">(445)</span></a></li>
                                                            <li><a href="#">Binoculars & Scopes <span class="wstcount">(45)</span></a>
                                                            </li>
                                                            <li><a href="#">Digital Cameras <span
                                                                        class="wstcount">(845)</span></a></li>
                                                            <li><a href="#">Film Photography <span class="wstcount">(245)</span></a>
                                                                <span class="wstmenutag bluetag">Popular</span></li>
                                                            <li><a href="#">Flashes <span class="wstcount">(105)</span></a>
                                                            </li>
                                                            <li><a href="#">Lenses <span
                                                                        class="wstcount">(445)</span></a></li>
                                                            <li><a href="#">Video <span
                                                                        class="wstcount">(145)</span></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li><img src="Assets/images/ele-menu-img03.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix">Cell Phones & Accessories
                                                            </li>
                                                            <li><a href="#">Unlocked Cell Phones </a></li>
                                                            <li><a href="#">Smartwatches </a></li>
                                                            <li><a href="#">Carrier Phones</a></li>
                                                            <li><a href="#">Cell Phone Cases</a> <span
                                                                    class="wstmenutag orangetag">Hot</span></li>
                                                            <li><a href="#">Bluetooth Headsets</a></li>
                                                            <li><a href="#">Cell Phone Accessories</a></li>
                                                            <li><a href="#">Fashion Tech</a></li>
                                                            <li><a href="#">Headphone</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li><img src="Assets/images/ele-menu-img04.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix">Wearable Device</li>
                                                            <li><a href="#">Activity Trackers </a></li>
                                                            <li><a href="#">Sports & GPS Watches</a></li>
                                                            <li><a href="#">Smart Watches</a> <span
                                                                    class="wstmenutag greentag">New</span></li>
                                                            <li><a href="#">Virtual Reality Headsets</a></li>
                                                            <li><a href="#">Wearable Cameras</a></li>
                                                            <li><a href="#">Smart Glasses</a></li>
                                                            <li><a href="#">Kids & Pets</a></li>
                                                            <li><a href="#">Smart Sport Accessories</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-laptop"></i>Computers &amp; Accessories</a>
                                        <div class="wstitemright clearfix computermenubg">
                                            <div class="wstmegamenucoll01 clearfix">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <div class="wstheading clearfix">Monitors <a href="#"
                                                                                                         class="wstmorebtn">View
                                                                    All</a></div>
                                                            <ul class="wstliststy03 clearfix">
                                                                <li><a href="#">50 Inches & Above <span
                                                                            class="wstmenutag greentag">New</span></a>
                                                                </li>
                                                                <li><a href="#">40 to 49.9 Inches </a></li>
                                                                <li><a href="#">30 to 39.9 Inches</a></li>
                                                                <li><a href="#">26 to 29.9 Inches</a></li>
                                                                <li><a href="#">18 to 19.9 Inches</a></li>
                                                                <li><a href="#">16 to 17.9 Inches</a></li>
                                                                <li><a href="#">26 to 29.9 Inches</a></li>
                                                                <li><a href="#">18 to 19.9 Inches</a></li>
                                                                <li><a href="#">16 to 17.9 Inches</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="wstheading clearfix">Software <a href="#"
                                                                                                         class="wstmorebtn">View
                                                                    All</a></div>
                                                            <ul class="wstliststy03 clearfix">
                                                                <li><a href="#">Antivirus & Security</a></li>
                                                                <li><a href="#">Business & Office</a> <span
                                                                        class="wstmenutag orangetag">Exclusive</span>
                                                                </li>
                                                                <li><a href="#">Web Design</a></li>
                                                                <li><a href="#">Digital Software</a></li>
                                                                <li><a href="#">Education & Reference</a></li>
                                                                <li><a href="#">Lifestyle & Hobbies</a></li>
                                                                <li><a href="#">Digital Software</a></li>
                                                                <li><a href="#">Education & Reference</a></li>
                                                                <li><a href="#">Lifestyle & Hobbies</a></li>
                                                            </ul>

                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="wstheading clearfix">Accessories <a href="#"
                                                                                                            class="wstmorebtn">View
                                                                    All</a></div>
                                                            <ul class="wstliststy03 clearfix">
                                                                <li><a href="#">Audio & Video Accessories</a></li>
                                                                <li><a href="#">Cable Security Devices</a></li>
                                                                <li><a href="#">Input Devices </a></li>
                                                                <li><a href="#">Memory Cards</a></li>
                                                                <li><a href="#">Monitor Accessories</a></li>
                                                                <li><a href="#">USB Gadgets</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-car"></i>Auto accessories</a>
                                        <div class="wstitemright clearfix wstpngsml">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy04 clearfix">
                                                            <li><img src="Assets/images/auto-menu-img01.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix"><a href="#">Interior</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy04 clearfix">
                                                            <li><img src="Assets/images/auto-menu-img02.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix"><a href="#">Styling</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy04 clearfix">
                                                            <li><img src="Assets/images/auto-menu-img03.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix"><a href="#">Utility</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy04 clearfix">
                                                            <li><img src="Assets/images/auto-menu-img04.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix"><a href="#">Spare Parts</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy04 clearfix">
                                                            <li><img src="Assets/images/auto-menu-img05.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix"><a href="#">Protection</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy04 clearfix">
                                                            <li><img src="Assets/images/auto-menu-img06.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix"><a href="#">Cleaning</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy04 clearfix">
                                                            <li><img src="Assets/images/auto-menu-img07.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix"><a href="#">Car Audio</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy04 clearfix">
                                                            <li><img src="Assets/images/auto-menu-img08.jpg" alt=" "></li>
                                                            <li class="wstheading clearfix"><a href="#">Gear &
                                                                    Accessories</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-heartbeat"></i>Health Care Products</a>
                                        <div class="wstitemright clearfix wstpngsml">
                                            <div class="container-fluid">


                                                <div class="row">
                                                    <div class="col-lg-4 col-md-12 wstmegamenucolr03 clearfix"><img
                                                            src="Assets/images/health-menu-img01.jpg" alt=""></div>
                                                    <div class="col-lg-8 col-md-12 clearfix">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="col-lg-4 col-md-12">
                                                                    <ul class="wstliststy05 clearfix">
                                                                        <li><img src="Assets/images/health-menu-img02.jpg"
                                                                                 alt=" "></li>
                                                                        <li class="wstheading clearfix">Health Care</li>
                                                                        <li><a href="#">Diabetes </a></li>
                                                                        <li><a href="#">Incontinence </a></li>
                                                                        <li><a href="#">Cough & Cold</a></li>
                                                                        <li><a href="#">Baby & Child Care</a> <span
                                                                                class="wstmenutag bluetag">Popular</span>
                                                                        </li>
                                                                        <li><a href="#">Women's Health</a></li>
                                                                        <li><a href="#">First Aid</a></li>
                                                                        <li><a href="#">Smoking Cessation</a></li>
                                                                        <li><a href="#">Sleep & Snoring</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12">
                                                                    <ul class="wstliststy05 clearfix">
                                                                        <li><img src="Assets/images/health-menu-img03.jpg"
                                                                                 alt=" "></li>
                                                                        <li class="wstheading clearfix">Personal Care
                                                                        </li>
                                                                        <li><a href="#">Shaving & Hair Removal</a></li>
                                                                        <li><a href="#">Feminine Hygiene</a></li>
                                                                        <li><a href="#">Oral Care</a></li>
                                                                        <li><a href="#">Foot Care</a> <span
                                                                                class="wstmenutag bluetag">Popular</span>
                                                                        </li>
                                                                        <li><a href="#">Hand Care</a></li>
                                                                        <li><a href="#">Personal Care Appliances</a>
                                                                        </li>
                                                                        <li><a href="#">Shaving Foams & Creams</a></li>
                                                                        <li><a href="#">Hair Removal Creams</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12">
                                                                    <ul class="wstliststy05 clearfix">
                                                                        <li><img src="Assets/images/health-menu-img04.jpg"
                                                                                 alt=" "></li>
                                                                        <li class="wstheading clearfix">Medical
                                                                            Equipment
                                                                        </li>
                                                                        <li><a href="#">Crepe Bandages, Tapes &
                                                                                Supplies </a></li>
                                                                        <li><a href="#">Neck Supports</a></li>
                                                                        <li><a href="#">Arm Supports</a> <span
                                                                                class="wstmenutag bluetag">Popular</span>
                                                                        </li>
                                                                        <li><a href="#">Elbow Braces</a></li>
                                                                        <li><a href="#">Knee & Leg Braces</a></li>
                                                                        <li><a href="#">Ankle Braces</a></li>
                                                                        <li><a href="#">Foot Supports</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li><a href="product.php" class="navtext"><span>Computer</span></a>
                        <div class="wsshoptabing wtsbrandmenu clearfix">
                            <div class="wsshoptabingwp clearfix">
                                <ul class="wstabitem02 clearfix">
                                    <li class="wsshoplink-active"><a href="#"><i class="fa fa-apple brandcolor01"
                                                                                 aria-hidden="true"></i>Apple</a>
                                        <div class="wsshoptab-active wstbrandbottom clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Apple Products</li>
                                                            <li><a href="#">MacBook </a> <span
                                                                    class="wstmenutag redtag">Popular</span></li>
                                                            <li><a href="#">MacBook Air 13-inch</a></li>
                                                            <li><a href="#">MacBook pro 13-inch</a></li>
                                                            <li><a href="#">MacBook pro 15-inch</a></li>
                                                            <li><a href="#">List Products 05</a></li>
                                                            <li><a href="#">List Products 06</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Apple More Products</li>
                                                            <li><a href="#">iMac 21.5-inch</a></li>
                                                            <li><a href="#">iMac 27.5-inch</a></li>
                                                            <li><a href="#">iMac pro</a></li>
                                                            <li><a href="#">iMac 13.5-inch</a></li>
                                                            <li><a href="#">List Products 11 </a></li>
                                                            <li><a href="#">List Products 12</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Apple More Products</li>
                                                            <li><a href="#">List Products 13 </a> <span
                                                                    class="wstmenutag orangetag">20% off</span></li>
                                                            <li><a href="#">List Products 14</a></li>
                                                            <li><a href="#">List Products 15</a></li>
                                                            <li><a href="#">List Products 16</a></li>
                                                            <li><a href="#">List Products 17</a></li>
                                                            <li><a href="#">List Products 18</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Apple More Products</li>
                                                            <li><a href="#">List Products 19 </a> <span
                                                                    class="wstmenutag bluetag">Winter Offer</span>
                                                            </li>
                                                            <li><a href="#">List Products 20</a></li>
                                                            <li><a href="#">List Products 21</a></li>
                                                            <li><a href="#">List Products 22</a></li>
                                                            <li><a href="#">List Products 23</a></li>
                                                            <li><a href="#">List Products 24</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-windows brandcolor02" aria-hidden="true"></i>
                                            Microsoft</a>
                                        <div class="wstbrandbottom clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Microsoft Products</li>
                                                            <li><a href="#">Microsoft Surface<span
                                                                        class="wstmenutag bluetag">Popular</span></a>
                                                            </li>
                                                            <li><a href="#">Microsoft Surface Pro 4 Tablet PC</a></li>
                                                            <li><a href="#">List Products 27</a> <span
                                                                    class="wstmenutag greentag">20% off</span></li>
                                                            <li><a href="#">Microsoft Surface Pro 3</a></li>
                                                            <li><a href="#">List Products 29</a></li>
                                                            <li><a href="#">List Products 30</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">MIcrosoft More Products</li>
                                                            <li><a href="#">Microsoft Surface Pro 4</a></li>
                                                            <li><a href="#">Microsoft Surface Pro 4 Tablet</a></li>
                                                            <li><a href="#">Microsoft Surface Pro 3 Tablet</a></li>
                                                            <li><a href="#">List Products 34</a></li>
                                                            <li><a href="#">List Products 35 </a></li>
                                                            <li><a href="#">List Products 36</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Microsoft More Products</li>
                                                            <li><a href="#">Microsoft Surface Pro</a></li>
                                                            <li><a href="#">Microsoft Surface Pro</a></li>
                                                            <li><a href="#">Microsoft Surface Pro</a></li>
                                                            <li><a href="#">List Products 40</a></li>
                                                            <li><a href="#">List Products 41</a></li>
                                                            <li><a href="#">List Products 42</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Micosoft More Products</li>
                                                            <li><a href="#">Microsoft Surface Pro</a></li>
                                                            <li><a href="#">Microsoft Surface Pro</a></li>
                                                            <li><a href="#">Microsoft Surface Pro</a></li>
                                                            <li><a href="#">List Products 46</a></li>
                                                            <li><a href="#">List Products 47</a></li>
                                                            <li><a href="#">List Products 48</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-desktop brandcolor03" aria-hidden="true"></i>
                                            Hp</a>
                                        <div class="wstbrandbottom clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">HP Products</li>
                                                            <li><a href="#">HP 15-bs632tu</a></li>
                                                            <li><a href="#">HP Pavilion 15-br077cl</a> <span
                                                                    class="wstmenutag redtag">Popular</span></li>
                                                            <li><a href="#">HP Pavilion cc142tx</a></li>
                                                            <li><a href="#">List Products 52</a></li>
                                                            <li><a href="#">List Products 53</a></li>
                                                            <li><a href="#">List Products 54</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Skype More Products</li>
                                                            <li><a href="#">HP Pavilion 15-cc112tu</a></li>
                                                            <li><a href="#">HP Probook 450 G5</a></li>
                                                            <li><a href="#">HP Pavilion 15-cc153tx</a></li>
                                                            <li><a href="#">List Products 58</a></li>
                                                            <li><a href="#">List Products 59 </a></li>
                                                            <li><a href="#">List Products 60</a> <span
                                                                    class="wstmenutag orangetag">20% off</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">HP More Products</li>
                                                            <li><a href="#">HP Pavilion 15-cc142tx</a></li>
                                                            <li><a href="#">HP Pavilion cc616tx</a></li>
                                                            <li><a href="#">List Products 63</a></li>
                                                            <li><a href="#">List Products 64</a></li>
                                                            <li><a href="#">List Products 65</a></li>
                                                            <li><a href="#">List Products 66</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">HP More Products</li>
                                                            <li><a href="#">HP Pavilion 15-cc154tx</a> <span
                                                                    class="wstmenutag bluetag">Winter Offer</span>
                                                            </li>
                                                            <li><a href="#">HP Omen AN025TX</a></li>
                                                            <li><a href="#">List Products 69</a></li>
                                                            <li><a href="#">List Products 70</a></li>
                                                            <li><a href="#">List Products 71</a></li>
                                                            <li><a href="#">List Products 72</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-desktop brandcolor04"
                                                       aria-hidden="true"></i>Asus</a>
                                        <div class="wstbrandbottom clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Asus Products</li>
                                                            <li><a href="#">ASUS ZenBook UX410UA<span
                                                                        class="wstmenutag bluetag">Popular</span></a>
                                                            </li>
                                                            <li><a href="#">Asus X540YA-E1-7010</a></li>
                                                            <li><a href="#">Asus X441SA Celeron</a></li>
                                                            <li><a href="#">List Products 76</a></li>
                                                            <li><a href="#">List Products 77</a></li>
                                                            <li><a href="#">List Products 78</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Asus More Products</li>
                                                            <li><a href="#">Asus VivoBook S510UA</a></li>
                                                            <li><a href="#">Asus S410UA</a></li>
                                                            <li><a href="#">List Products 81</a></li>
                                                            <li><a href="#">List Products 82</a></li>
                                                            <li><a href="#">List Products 83 <span
                                                                        class="wstmenutag greentag">20% off</span></a>
                                                            </li>
                                                            <li><a href="#">List Products 84</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Asus More Products</li>
                                                            <li><a href="#">Asus VivoBook X542UN</a></li>
                                                            <li><a href="#">Asus VivoBook X441NA</a></li>
                                                            <li><a href="#">List Products 87</a></li>
                                                            <li><a href="#">List Products 89</a></li>
                                                            <li><a href="#">List Products 90</a></li>
                                                            <li><a href="#">List Products 91</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Asus More Products</li>
                                                            <li><a href="#">Asus X441UA</a></li>
                                                            <li><a href="#">Asus VivoBook Flip TP203NAH</a></li>
                                                            <li><a href="#">Asus VivoBook Max X441UA</a></li>
                                                            <li><a href="#">List Products 95</a></li>
                                                            <li><a href="#">List Products 96</a></li>
                                                            <li><a href="#">List Products 97</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-desktop brandcolor05" aria-hidden="true"></i>Lenovo</a>
                                        <div class="wstbrandbottom clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Amazon Products</li>
                                                            <li><a href="#">List Products 98 </a></li>
                                                            <li><a href="#">List Products 99</a></li>
                                                            <li><a href="#">List Products 00</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 02</a> <span
                                                                    class="wstmenutag redtag">Popular</span></li>
                                                            <li><a href="#">List Products 03</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Amazon More Products</li>
                                                            <li><a href="#">List Products 04 <span
                                                                        class="wstmenutag orangetag">20% off</span></a>
                                                            </li>
                                                            <li><a href="#">List Products 05</a></li>
                                                            <li><a href="#">List Products 06</a></li>
                                                            <li><a href="#">List Products 07</a></li>
                                                            <li><a href="#">List Products 08 </a></li>
                                                            <li><a href="#">List Products 09</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Amazon More Products</li>
                                                            <li><a href="#">List Products 10 </a></li>
                                                            <li><a href="#">List Products 11</a></li>
                                                            <li><a href="#">List Products 12</a></li>
                                                            <li><a href="#">List Products 13</a></li>
                                                            <li><a href="#">List Products 14</a></li>
                                                            <li><a href="#">List Products 15</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Amazon More Products</li>
                                                            <li><a href="#">List Products 16 </a> <span
                                                                    class="wstmenutag bluetag">Winter Offer</span>
                                                            </li>
                                                            <li><a href="#">List Products 17</a></li>
                                                            <li><a href="#">List Products 18</a></li>
                                                            <li><a href="#">List Products 19</a></li>
                                                            <li><a href="#">List Products 20</a></li>
                                                            <li><a href="#">List Products 21</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-desktop brandcolor06"></i>Dell</a>
                                        <div class="wstbrandbottom clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Dell Products</li>
                                                            <li><a href="#"> Dell Latitude 3480 <span
                                                                        class="wstmenutag bluetag">Popular</span></a>
                                                            </li>
                                                            <li><a href="#">Dell Vostro V3458</a></li>
                                                            <li><a href="#">Dell Inspiron 5567</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Dell More Products
                                                            </li>
                                                            <li><a href="#">Dell Inspiron N3467</a></li>
                                                            <li><a href="#">Dell Inspiron 13-5379</a></li>
                                                            <li><a href="#">Dell Inspiron 15-3567</a></li>
                                                            <li><a href="#">List Products 10</a></li>
                                                            <li><a href="#">List Products 11 <span
                                                                        class="wstmenutag greentag">20% off</span></a>
                                                            </li>
                                                            <li><a href="#">List Products 12</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Dell More Products
                                                            </li>
                                                            <li><a href="#">Dell Inspiron 356</a></li>
                                                            <li><a href="#">Dell Inspiron-15 5570</a></li>
                                                            <li><a href="#">Dell Latitude 7250</a></li>
                                                            <li><a href="#">List Products 16</a></li>
                                                            <li><a href="#">List Products 17</a></li>
                                                            <li><a href="#">List Products 18</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Dell More Products
                                                            </li>
                                                            <li><a href="#">Dell Inspiron 15-5567</a></li>
                                                            <li><a href="#">Dell Inspiron 15-5568</a></li>
                                                            <li><a href="#">Dell Inspiron 15-5567</a></li>
                                                            <li><a href="#">List Products 22</a></li>
                                                            <li><a href="#">List Products 23</a></li>
                                                            <li><a href="#">List Products 24</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-desktop brandcolor07"></i>Acer</a>
                                        <div class="wstbrandbottom clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Houzz Products</li>
                                                            <li><a href="#">List Products 01 </a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 01</a> <span
                                                                    class="wstmenutag redtag">Popular</span></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Houzz More Products</li>
                                                            <li><a href="#">List Products 07 <span
                                                                        class="wstmenutag orangetag">20% off</span></a>
                                                            </li>
                                                            <li><a href="#">List Products 08</a></li>
                                                            <li><a href="#">List Products 09</a></li>
                                                            <li><a href="#">List Products 10</a></li>
                                                            <li><a href="#">List Products 11 </a></li>
                                                            <li><a href="#">List Products 12</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Houzz More Products</li>
                                                            <li><a href="#">List Products 13 </a></li>
                                                            <li><a href="#">List Products 14</a></li>
                                                            <li><a href="#">List Products 15</a></li>
                                                            <li><a href="#">List Products 16</a></li>
                                                            <li><a href="#">List Products 17</a></li>
                                                            <li><a href="#">List Products 18</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Houzz More Products</li>
                                                            <li><a href="#">List Products 19 </a> <span
                                                                    class="wstmenutag bluetag">Winter Offer</span>
                                                            </li>
                                                            <li><a href="#">List Products 20</a></li>
                                                            <li><a href="#">List Products 21</a></li>
                                                            <li><a href="#">List Products 22</a></li>
                                                            <li><a href="#">List Products 23</a></li>
                                                            <li><a href="#">List Products 24</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#"><i class="fa fa-desktop brandcolor08"></i>Samsung</a>
                                        <div class="wstbrandbottom clearfix">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Get Pocket Products</li>
                                                            <li><a href="#">List Products 01 <span
                                                                        class="wstmenutag bluetag">Popular</span></a>
                                                            </li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                            <li><a href="#">List Products 01</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Get Pocket More Products
                                                            </li>
                                                            <li><a href="#">List Products 07 </a></li>
                                                            <li><a href="#">List Products 08</a></li>
                                                            <li><a href="#">List Products 09</a></li>
                                                            <li><a href="#">List Products 10</a></li>
                                                            <li><a href="#">List Products 11 <span
                                                                        class="wstmenutag greentag">20% off</span></a>
                                                            </li>
                                                            <li><a href="#">List Products 12</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Get Pocket More Products
                                                            </li>
                                                            <li><a href="#">List Products 13 </a></li>
                                                            <li><a href="#">List Products 14</a></li>
                                                            <li><a href="#">List Products 15</a></li>
                                                            <li><a href="#">List Products 16</a></li>
                                                            <li><a href="#">List Products 17</a></li>
                                                            <li><a href="#">List Products 18</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12">
                                                        <ul class="wstliststy02 clearfix">
                                                            <li class="wstheading clearfix">Get Pocket More Products
                                                            </li>
                                                            <li><a href="#">List Products 19 </a></li>
                                                            <li><a href="#">List Products 20</a></li>
                                                            <li><a href="#">List Products 21</a></li>
                                                            <li><a href="#">List Products 22</a></li>
                                                            <li><a href="#">List Products 23</a></li>
                                                            <li><a href="#">List Products 24</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li><a href="product.php" class="navtext"><span>Tools</span></a>
                        <div class="megamenu clearfix">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-3 col-md-12">
                                        <ul class="wstliststy02 clearfix">
                                            <li class="wstheading clearfix"> Skype Products</li>
                                            <li><a href="#">List Products 01 </a> <span class="wstmenutag redtag">Popular</span>
                                            </li>
                                            <li><a href="#">List Products 02</a></li>
                                            <li><a href="#">List Products 03</a></li>
                                            <li><a href="#">List Products 04</a></li>
                                            <li><a href="#">List Products 05</a></li>
                                            <li><a href="#">List Products 06</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <ul class="wstliststy02 clearfix">
                                            <li class="wstheading clearfix">Paypal Products</li>
                                            <li><a href="#">List Products 07 </a></li>
                                            <li><a href="#">List Products 08</a></li>
                                            <li><a href="#">List Products 09</a></li>
                                            <li><a href="#">List Products 10</a></li>
                                            <li><a href="#">List Products 11 </a></li>
                                            <li><a href="#">List Products 12</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <ul class="wstliststy02 clearfix">
                                            <li class="wstheading clearfix">Sound Cloud Products</li>
                                            <li><a href="#">List Products 13 </a> <span class="wstmenutag orangetag">20% off</span>
                                            </li>
                                            <li><a href="#">List Products 14</a></li>
                                            <li><a href="#">List Products 15</a></li>
                                            <li><a href="#">List Products 16</a></li>
                                            <li><a href="#">List Products 17</a></li>
                                            <li><a href="#">List Products 18</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <ul class="wstliststy02 clearfix">
                                            <li class="wstheading clearfix">Get Pocket Products</li>
                                            <li><a href="#">List Products 19 </a> <span class="wstmenutag bluetag">Winter Offer</span>
                                            </li>
                                            <li><a href="#">List Products 20</a></li>
                                            <li><a href="#">List Products 21</a></li>
                                            <li><a href="#">List Products 22</a></li>
                                            <li><a href="#">List Products 23</a></li>
                                            <li><a href="#">List Products 24</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="product.php" class="navtext"><span>Headphones</span></a>
                        <div class="megamenu clearfix halfmenu">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-6 col-md-12">
                                        <ul class="wstliststy06 clearfix">
                                            <li class="wstheading clearfix">Windows Products</li>
                                            <li><a href="#">List Products 01 </a> <span class="wstmenutag redtag">Popular</span>
                                            </li>
                                            <li><a href="#">List Products 02</a></li>
                                            <li><a href="#">List Products 03</a></li>
                                            <li><a href="#">List Products 04</a></li>
                                            <li><a href="#">List Products 05</a></li>
                                            <li><a href="#">List Products 06</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        <ul class="wstliststy06 clearfix">
                                            <li class="wstheading clearfix">Apple More Products</li>
                                            <li><a href="#">List Products 07 </a></li>
                                            <li><a href="#">List Products 08</a></li>
                                            <li><a href="#">List Products 09</a></li>
                                            <li><a href="#">List Products 10</a></li>
                                            <li><a href="#">List Products 11 </a></li>
                                            <li><a href="#">List Products 12</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="contact.php" class="navtext"><span>Contact Us</span></a>
                    </li>
                    <li>
                        <a href="index.php" class="navtext"><span>Offer Zone</span></a>
                    </li>

                </ul>
            </nav>
        </div>
        <!--Menu HTML Code-->
    </div>

</div>