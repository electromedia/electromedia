<div class="product_header">
    <div class="container">
        <h2>Top Rated Products</h2>
    </div>
</div>
<div class="product_list">
    <div class="container-fluid">

        <div data-slick='{"slidesToShow": 4, "slidesToScroll": 1}' class="slick hidden-xs hidden-sm">
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <div class="tag sale"><span>Sale</span></div>
                        <img src="Assets/images/product/headphone/bt2.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt3.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt4.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt3.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt2.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div data-slick='{"slidesToShow": 1, "slidesToScroll": 1}' class="slick visible-xs text-center">
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <div class="tag sale"><span>Sale</span></div>
                        <img src="Assets/images/product/headphone/bt2.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt3.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt4.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt3.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt2.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div data-slick='{"slidesToShow": 2, "slidesToScroll": 1}' class="slick visible-sm text-center">
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <div class="tag sale"><span>Sale</span></div>
                        <img src="Assets/images/product/headphone/bt2.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt3.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt4.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt3.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/headphone/bt2.jpg" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>