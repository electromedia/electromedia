<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."project".DIRECTORY_SEPARATOR."ControlPanel".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";
use rashed\Utility\Products;
$products=new Products();
$result=$products->frontView('Featured Products');
?>


<div class="product_header">
    <div class="container">
        <h2>Featured products</h2>
    </div>
</div>
<div class="product_list">
    <div class="container-fluid">

        <div data-slick='{"slidesToShow": 4, "slidesToScroll": 1}' class="slick hidden-xs hidden-sm">

            <?php
                foreach ($result as $product):
            ?>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="http://localhost/project/ControlPanel/Assets/uploads/products/<?php echo $product['picture']?>" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#"><?= $product['title']?></a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. <?= $product['mrp']?> tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                endforeach;
            ?>

        </div>
        <div data-slick='{"slidesToShow": 1, "slidesToScroll": 1}' class="slick visible-xs text-center">
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m1.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <div class="tag sale"><span>Sale</span></div>
                        <img src="Assets/images/product/mobile/m2.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m3.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m4.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m5.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m6.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div data-slick='{"slidesToShow": 2, "slidesToScroll": 1}' class="slick visible-sm text-center">
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m1.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <div class="tag sale"><span>Sale</span></div>
                        <img src="Assets/images/product/mobile/m2.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m3.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m4.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m5.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <div class="single_product">
                    <div class="">
                        <div class="tag hot"><span>Hot</span></div>
                        <div class="img_hover">
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><i
                                    class="fa fa-shopping-cart"></i></a>
                            <a href="discryption.php"><i class="fa fa-search"></i></a>
                            <a href="#"><i class="fa fa-heart"></i></a>
                            <a href="#"><i class="fa fa-signal"></i></a>
                        </div>
                        <img src="Assets/images/product/mobile/m6.png" alt="" class="img-thumbnail">
                        <div class="img_details">
                            <span><a href="#">HeadPhone</a></span>
                            <div class="product_icon">
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star"></i></span>
                                <span><i class="fa fa-star-o"></i></span>
                            </div>
                            <p>Price: BDT. 780 tk.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>