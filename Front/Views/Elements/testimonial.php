<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Testimonial;
$testimonial=new Testimonial();
$result=$testimonial->frontView();



?>


<div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        $active = '';
        for ($i = 0; $i < count($result); $i++):
        ?>
            <li data-target="#carousel-example" data-slide-to="0" class="<?= $active?>"></li>
        <?php
            $active='';
            endfor;
        ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php
        $active = 'active';
        foreach ($result as $testimonial):
            ?>
            <div class="item <?= $active ?>">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-item">
                            <div class="photo">
                                <img src="http://localhost/project/ControlPanel/Assets/uploads/testimonial/<?= $testimonial['picture'] ?>"
                                     class="img-responsive text-center" alt="a"/>
                            </div>
                            <div class="info">
                                <div class="row">
                                    <div class="img_details text-center testimonial">
                                        <P><?= $testimonial['body'] ?></P>
                                        <h5><?= $testimonial['name'] ?></h5>
                                        <h6><?= $testimonial['designation'] ?></h6>
                                    </div>

                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $active = '';
        endforeach;
        ?>
    </div>
</div>