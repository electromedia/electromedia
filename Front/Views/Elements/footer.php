<footer id="footer-main" class="footer-main container-fluid">
    <!-- Container -->
    <div class="container">
        <div class="row">
            <!-- Widget About -->
            <aside class="col-md-3 col-sm-6 col-xs-12 ftr-widget widget_about">
                <a href="../index.php" title="Electronic Media">Electro<span>Media</span></a>
                <div class="info">
                    <p><i class="fa fa-hand-pointer-o"></i>Electromedia, Chakbazer, Chittagong.</p>
                    <p><i class="fa fa-phone"></i><a href="tel:(11)1234567890" title="Phone" class="phone">018 523 468 41</a></p>
                    <p><i class="fa fa-envelope"></i><a href="mailto:rashedctg1719@gmail.com" title="rashedctg1719@gmail.com">rashedctg1719@gmail.com</a></p>
                </div>
            </aside><!-- Widget About /- -->
            <!-- Widget Links -->
            <aside class="col-md-3 col-sm-6 col-xs-12 ftr-widget widget_links">
                <h3 class="widget-title">Popular Links</h3>
                <ul>
                    <li><a href="#" title="Popular Products">Popular Products</a></li>
                    <li><a href="#" title="Best Selling">Best Selling</a></li>
                    <li><a href="#" title="About Us">About Us</a></li>
                    <li><a href="#" title="Deal Of The Day">Deal Of The Day</a></li>
                    <li><a href="#" title="Product Collections">Product Collections</a></li>
                    <li><a href="#" title="Contact Us">Contact Us</a></li>
                </ul>
            </aside><!-- Widget Links /- -->
            <!-- Widget Account -->
            <aside class="col-md-3 col-sm-6 col-xs-12 ftr-widget widget_links widget_account">
                <h3 class="widget-title">my account</h3>
                <ul>
                    <li><a href="#" title="My Order Details">My Order Details</a></li>
                    <li><a href="#" title="My credit Offers">My credit Offers</a></li>
                    <li><a href="#" title="My addresses">My addresses</a></li>
                    <li><a href="#" title="My Personal Details">My Personal Details</a></li>
                    <li><a href="#" title="My Account Details">My Account Details</a></li>
                </ul>
            </aside><!-- Widget Account /- -->
            <!-- Widget Newsletter -->
            <aside class="col-md-3 col-sm-6 col-xs-12 ftr-widget widget_newsletter">
                <h3 class="widget-title">newsletter</h3>
                <div class="input-group">
                    <input class="form-control" placeholder="email address" type="text">
                    <span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-paper-plane-o"></i></button>
							</span>
                </div>
                <h5>Get New Updates From Our Collections</h5>
                <h3 class="widget-title widget-title-1">Follow Us</h3>
                <ul class="social">
                    <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#" title="Tumblr"><i class="fa fa-tumblr"></i></a></li>
                    <li><a href="#" title="Vimeo"><i class="fa fa-vimeo"></i></a></li>
                </ul>
            </aside><!-- Widget Newsletter /- -->
        </div>
        <div class="copyright-section">
            <div class="coyright-content">
                <p class="text-center">&copy; All Right reserved by Electromedia team.</p>
            </div>
        </div>
    </div><!-- Container /- -->
</footer><!-- Footer Main /- -->

