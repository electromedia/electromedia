<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/project/ControlPanel/vendor/autoload.php";
use rashed\Utility\Categorie;
$result=new Categorie();
$row=$result->frontCategory();


?>


<h3 class="side-title text-uppercase">Categories</h3>
<ul class="list-unstyled">
    <?php
    foreach ($row as $categories):
        ?>
        <li><a href="<?= $categories['link']?>" title=""><?= $categories['name']?></a></li>
    <?php
    endforeach;

    ?>
</ul>