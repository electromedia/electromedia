<h3 class="text-uppercase side-title">choose color</h3>
<!-- start color changer -->
<div class="color-changer">
    <div class="red "></div>
    <div class="purple"></div>
    <div class="darkcyan"></div>
    <div class="blue"></div>
    <div class="deeppink"></div>
    <div class="green"></div>
    <div class="chocolate"></div>
</div><!-- end color changer -->