<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
use rashed\Utility\Brands;
$brands=new Brands();
$result=$brands->frontView();
?>

<h3 class="text-uppercase side-title">brands</h3>

<?php
    foreach ($result as $brand):
?>
<p><input type="checkbox" id="<?= $brand['title']?>" name="<?= $brand['title']?>" value=""><label for="<?= $brand['title']?>" class="text-uppercase"><?= $brand['title']?></label>
</p>
<?php
    endforeach;
?>