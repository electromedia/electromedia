<!-- JS -->
<script src="Assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="Assets/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="Assets/js/bootstrap.min.js"></script>

<script src="Assets/js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="Assets/plugins/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="Assets/plugins/slick/slick.min.js"></script>
<script type="text/javascript" src="Assets/js/custom.js"></script>

<!--initalize elevatezoom plugin-->
<script type="text/javascript">
    $(document).ready(function () {
        $("#zoom_03").elevateZoom({ zoomType: "inner", zoomWindowFadeIn: 500, zoomWindowFadeOut: 750, cursor: "crosshair", gallery:'gallery_01', galleryActiveClass: "active", imageCrossfade: true, loadingIcon: "http://www.elevateweb.co.uk/spinner.gif"});

        $("#zoom_03").bind("click", function(e) {
            var ez =   $('#zoom_03').data('elevateZoom');
            ez.closeAll(); //NEW: This function force hides the lens, tint and window
            $.fancybox(ez.getGalleryList());
            return false;
        });

    });

</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#img_01").elevateZoom({gallery:'gal1', cursor: 'pointer', galleryActiveClass: "active", imageCrossfade: true, loadingIcon: "http://www.elevateweb.co.uk/spinner.gif"});

        $("#img_01").bind("click", function(e) {
            var ez =   $('#img_01').data('elevateZoom');
            ez.closeAll(); //NEW: This function force hides the lens, tint and window
            $.fancybox(ez.getGalleryList());
            return false;
        });

    });

</script>

<!--owl Carousel-->
<script>
    $(document).ready(function () {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items: 4,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            autoplayHoverPause: true
        });
    })
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.slick').slick();
    });
</script>

<!--msdropdown-->
<script src="Assets/plugins/msdropdown/jquery.dd.min.js" type="text/javascript"></script>
<script language="javascript">
    $(document).ready(function(e) {
        try {
            $("body select").msDropDown();
        } catch(e) {
            alert(e.message);
        }
    });
</script>


</body>
</html>