<h3 class="text-uppercase side-title">best sellers</h3>

<div class="row">

    <div class="col-sm-12">
        <!-- Controls -->
        <div class="controls pull-right hidden-xs">
            <a class="left fa  btn btn-success" href="#carousel-example"
               data-slide="prev"><span>&laquo;</span></a><a class="right fa  btn btn-success"
                                                            href="#carousel-example"
                                                            data-slide="next"><span>&raquo;</span></a>
        </div>
    </div>
</div>
<div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-item">
                        <div class="photo">
                            <img src="Assets/images/product/mobile/m1.png" class="img-responsive" alt="a"/>
                        </div>
                        <div class="info">
                            <div class="row">
                                <div class="img_details text-center">
                                    <span><a href="#">HeadPhone</a></span>
                                    <div class="product_icon">
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                    </div>
                                    <p>Price: BDT. 780 tk.</p>
                                </div>

                            </div>
                            <div class="img_hover text-center">
                                <a href="#" tabindex="0" data-toggle="modal"
                                   data-target=".bs-example-modal-lg"><i
                                        class="fa fa-shopping-cart"></i></a>
                                <a href="discryption.php" tabindex="0"><i class="fa fa-search"></i></a>
                                <a href="#" tabindex="0"><i class="fa fa-heart"></i></a>
                                <a href="#" tabindex="0"><i class="fa fa-signal"></i></a>
                            </div>
                            <div class="clearfix">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-item">
                        <div class="photo">
                            <img src="Assets/images/product/computer/c5.jpg" class="img-responsive" alt="a"/>
                        </div>
                        <div class="info">
                            <div class="row">
                                <div class="img_details text-center">
                                    <span><a href="#">HeadPhone</a></span>
                                    <div class="product_icon">
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                    </div>
                                    <p>Price: BDT. 780 tk.</p>
                                </div>

                            </div>
                            <div class="img_hover text-center">
                                <a href="#" tabindex="0" data-toggle="modal"
                                   data-target=".bs-example-modal-lg"><i
                                        class="fa fa-shopping-cart"></i></a>
                                <a href="discryption.php" tabindex="0"><i class="fa fa-search"></i></a>
                                <a href="#" tabindex="0"><i class="fa fa-heart"></i></a>
                                <a href="#" tabindex="0"><i class="fa fa-signal"></i></a>
                            </div>
                            <div class="clearfix">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-item">
                        <div class="photo">
                            <img src="Assets/images/product/headphone/bt.jpg" class="img-responsive" alt="a"/>
                        </div>
                        <div class="info">
                            <div class="row">
                                <div class="img_details text-center">
                                    <span><a href="#">HeadPhone</a></span>
                                    <div class="product_icon">
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star-o"></i></span>
                                    </div>
                                    <p>Price: BDT. 780 tk.</p>
                                </div>

                            </div>
                            <div class="img_hover text-center">
                                <a href="#" tabindex="0" data-toggle="modal"
                                   data-target=".bs-example-modal-lg"><i
                                        class="fa fa-shopping-cart"></i></a>
                                <a href="discryption.php" tabindex="0"><i class="fa fa-search"></i></a>
                                <a href="#" tabindex="0"><i class="fa fa-heart"></i></a>
                                <a href="#" tabindex="0"><i class="fa fa-signal"></i></a>
                            </div>
                            <div class="clearfix">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>