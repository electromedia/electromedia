<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
?>
<?= $appconfig->element('head.php'); ?>
<?= $appconfig->element('header_and_nav.php'); ?>

<?= $appconfig->element('nav.php'); ?>

||##CONTENT##||

<?php

//cart-modal
echo $appconfig->element('cart_modal.php');

//sponser
echo $appconfig->element('sponser.php');

//footer
echo $appconfig->element('footer.php');

//js
echo $appconfig->element('js.php');

?>
