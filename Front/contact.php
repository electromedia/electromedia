<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/project/ControlPanel/vendor/autoload.php";
session_start();
use rashed\Utility\AppConfig;
use rashed\Utility\Contact;
use rashed\Utility\Message;

$appconfig = new AppConfig();

echo $appconfig->element('head.php');
echo $appconfig->element('header_and_nav.php');
echo $appconfig->element('nav.php');

?>


    <!--custom map-->
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7496148.87612487!2d85.8469299348491!3d23.452204873280778!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30adaaed80e18ba7%3A0xf2d28e0c4e1fc6b!2sBangladesh!5e0!3m2!1sen!2sbd!4v1507035724348"
            width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>

    <!--contact section start-->
    <section id="contact">
        <div class="container">
            <div class="row">

                <!--school address-->
                <div class="col-md-5 col-sm-5 col-xs-12 col-lg-5">

                    <div class="address">

                        <!--address-->
                        <div class="row">

                            <!--address icon-->
                            <div class="contact_1">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </div>

                            <!--address details-->
                            <div class="content">
                                <h3 class="text-center">Address</h3>
                                <P class="text-center">140,Bangladesh</P>
                                <p class="text-center">Chittagong,Chakbazar</p>
                            </div>
                        </div>

                        <!--school phone -->
                        <div class="row">

                            <!--phone icon-->
                            <div class="contact_1">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>

                            <!--phone number-->
                            <div class="content">
                                <h3 class="text-center">Phone</h3>
                                <P class="text-center">+880 185 234 6841</P>
                            </div>
                        </div>

                        <!--globee address-->
                        <div class="row">

                            <!--icon-->
                            <div class="contact_1">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                            </div>

                            <!--details-->
                            <div class="content">
                                <h3 class="text-center">Address</h3>
                                <P class="text-center">140,Bangladesh</P>
                                <p class="text-center">Chittagong,Chakbazer</p>
                            </div>
                        </div>
                    </div>
                </div>

                <!--form start here-->
                <div class="col-md-7 col-sm-7 col-xs-12">

                    <!--content-->
                    <h3>Leave a Message</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos facere quod veniam.
                        Laborum, nostrum, odio!</p>

                    <!--form-->
                    <form action="form_action.php" method="post">
                        <div class="row">
                            <?php
                            $message = new Message();
                            if ($message->has()):
                                ?>
                                <div class="alert alert-success">
                                    <?php echo $message->get(); ?>
                                </div>
                            <?php
                            endif;
                            ?>
                            <!--name-->
                            <div class="col-md-12">
                                <label for="name">Name</label>
                                <input name="name" id="name" type="text" class="form-control">
                            </div>

                            <!--email-->
                            <div class="col-md-12">
                                <label for="email">Email</label>
                                <input name="email" id="email" type="text" class="form-control">
                            </div>

                            <!--comment here-->
                            <div class="col-md-12">
                                <label for="subject">Subject</label>
                                <input name="subject" id="subject" type="text" class="form-control">

                                <label for="message">Massage</label>
                                <textarea name="message" id="message" cols="10" rows="10"
                                          class="form-control"></textarea>
                            </div>
                        </div>

                        <!--send message-->
                        <button type="submit">Send Message</button>
                    </form>

                </div>
            </div>
        </div>
    </section>


<?php
echo $appconfig->element('sponser.php');
echo $appconfig->element('footer.php');
echo $appconfig->element('js.php');
?>