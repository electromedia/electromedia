<?php
session_start();
if (!array_key_exists('sId',$_SESSION)){
    $_SESSION['sId']=rand(1000,100000);
}

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

$layout = file_get_contents($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "Front" . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . "Themes" . DIRECTORY_SEPARATOR . "ThemeB" . DIRECTORY_SEPARATOR . "layout.php");

//$layout = include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "Front" . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . "Themes" . DIRECTORY_SEPARATOR . "ThemeB" . DIRECTORY_SEPARATOR . "layout.php");

use rashed\Utility\AppConfig;
use rashed\Utility\Debug;

$appconfig = new AppConfig();

use rashed\Utility\Products;

$product = new Products();

ob_start();


?>


<!--Computer-->
<section id="product">
    <div class="container">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><!-- col-sm-3 col-md-3 Starts -->

            <!-- Category -->
            <div class="single category popular">
                <?= $appconfig->element('categories.php'); ?>
            </div> <!-- Category  End -->

            <!--Best seller single product slider-->
            <div class="single category popular_products">
                <?= $appconfig->element('bestSeller_singleProduct_slider.php'); ?>
            </div>


            <!--Currency switcher-->
            <div id="" class="clearfix">
                <div class="currency-switcher">
                    <?= $appconfig->element('currency_switcher.php'); ?>
                </div>

            </div>

            <!--prices range starts-->
            <div class="single category prices">
                <?= $appconfig->element('price_range.php'); ?>
            </div>
            <!--prices ends-->

            <!--brands starts-->
            <div class="single category brands">
                <?= $appconfig->element('product_brands.php'); ?>
            </div>
            <!--brands ends-->

            <!--color starts-->
            <div class="single category choose_color">
                <?= $appconfig->element('color.php'); ?>

            </div>


            <!--popular tags starts-->
            <aside class="widget widget_tags">
                <?= $appconfig->element('popular_tag.php'); ?>
            </aside>
            <!--popular tags ends-->


            <!--paying system-->
            <div class=" single category pay">
                <?= $appconfig->element('paying_gateway.php'); ?>
            </div>


        </div>
        <div class="col-md-9 col-xs-12 col-sm-9 col-lg-9">
            <!--Slider start-->
            <?= $appconfig->element('banner.php'); ?>
            <!--Slider end-->

            <!--New Products-->
            <?= $appconfig->element('new_products.php'); ?>

            <!--Featured Products Banner-->
            <div class="banner">
                <img src="Assets/images/banner/mobile.jpg" alt="" class="img-responsive mobile_banner">
            </div>

            <!--Featured Products-->
            <?= $appconfig->element('featured_products.php') ?>

            <!--Top rated products banner-->
            <div class="banner">
                <img src="Assets/images/banner/headphone.png" alt="" class="img-responsive">
            </div>

            <!--Top rated products-->
            <?= $appconfig->element('top_rated_products.php') ?>

            <!--Best seller banner-->
            <div class="banner">
                <img src="Assets/images/banner/tv.jpg" alt="" class="img-responsive">
            </div>

            <!--Best seller products-->
            <?= $appconfig->element('best_seller.php') ?>
        </div>
    </div>
</section>


<?php
$content = ob_get_contents();
ob_end_clean();

$head = $appconfig->element('head.php');
$sidebar = $appconfig->element('header_and_nav.php');
$nav = $appconfig->element('nav.php');





$output = str_replace(array('|##HTMLHEAD##|','|##HEADERANDNAV##|','|##NAVBAR##|','|##CONTENT##|'), array($head,$sidebar,$nav,$content), $layout);

echo $output;
echo $appconfig->element('cart_modal.php');
echo $appconfig->element('sponser.php');
echo $appconfig->element('footer.php');
echo $appconfig->element('js.php');

?>



