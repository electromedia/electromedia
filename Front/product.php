<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/project/ControlPanel/vendor/autoload.php';
session_start();
use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
echo $appconfig->element('head.php');
echo $appconfig->element('header_and_nav.php');
echo $appconfig->element('nav.php');
?>

<!--Computer-->
<section id="product">
    <div class="container">
        <div class="col-xs-12 col-sm-3 col-md-3"><!-- col-sm-3 col-md-3 Starts -->

            <!-- Category -->
            <div class="single category popular">
                <?php echo $appconfig->element('categories.php'); ?>
            </div> <!-- Category  End -->

            <!--hot deals-->
            <div class="single category popular_products">
                <?php echo $appconfig->element('hot_deals.php'); ?>
            </div>
            <!-- Hot deals End-->

            <!--brands starts-->
            <div class="single category brands">
                <?php echo $appconfig->element('product_brands.php'); ?>
            </div>
            <!--brands ends-->

            <!--testimonial-->
            <div class="single category popular_products tst">
                <?php echo $appconfig->element('testimonial.php') ?>
            </div>

            <!--popular tags starts-->
            <aside class="widget widget_tags">
                <?php echo $appconfig->element('popular_tag.php') ?>
            </aside>
            <!--popular tags ends-->

            <!--color starts-->
            <div class="single category choose_color">
                <?php echo $appconfig->element('color.php'); ?>
            </div>
        </div>

        <div class="col-md-9">

            <!--Slider-->
            <div class="bt-carousel">
                <?php echo $appconfig->element('banner.php'); ?>

                <!--sorting-->
                <div class="row sort">
                    <div class="col-md-offset-2 col-md-5 text-center">
                        <span>Sort by:</span>
                        <div class="dropdown inline">
                            <select name="orderby" class="orderby btn dropdown">
                                <option value="Default sorting" selected="selected">Default sorting</option>
                                <option value="">Sort by popularity</option>
                                <option value="">Sort by average rating</option>
                                <option value="">Sort by newness</option>
                                <option value="">Sort by price: low to high</option>
                                <option value="">Sort by price: high to low</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5 text-center">
                        <span>Show:</span>
                        <div class="dropdown inline">
                            <select name="orderby" class="orderby btn dropdown">
                                <option value="Default sorting" selected="selected">12</option>
                                <option value="">24</option>
                                <option value="">48</option>
                                <option value="">96</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>


            <!--New Products-->
            <?= $appconfig->element('new_products.php') ?>

            <!--featured Products-->
            <div class="banner">
                <!--                <img src="images/banner/mobile.jpg" alt="" class="img-responsive mobile_banner">-->
            </div>
            <?= $appconfig->element('featured_products.php') ?>

            <!--Top rated products-->
            <div class="banner">
                <img src="Assets/images/banner/headphone.png" alt="" class="img-responsive">
            </div>
            <?= $appconfig->element('top_rated_products.php') ?>

            <!--best seller-->
            <div class="banner">
                <img src="Assets/images/banner/tv.jpg" alt="" class="img-responsive">
            </div>
            <?= $appconfig->element('best_seller.php') ?>

        </div>
    </div>
</section>


<?php
//cart-modal
echo $appconfig->element('cart_modal.php');

echo $appconfig->element('sponser.php');

echo $appconfig->element('footer.php');

echo $appconfig->element('js.php');

?>
