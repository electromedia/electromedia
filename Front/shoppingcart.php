<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/project/ControlPanel/vendor/autoload.php';

use rashed\Utility\AppConfig;
use rashed\Utility\Cart;
use rashed\Utility\Debug;

$user=$_SESSION['sId'];

$cart = new Cart();
$result = $cart->listIndex($user);
$appconfig = new AppConfig();
echo $appconfig->element('head.php');
echo $appconfig->element('header_and_nav.php');
echo $appconfig->element('nav.php');
?>

<div class="chackout">
    <div class="container">
        <div class="row">
            <div class="col-md-8 lside">
                <h2>Shopping Cart</h2>
                <table class="table">
                    <?php
                    if (count($result) > 0) {
                        foreach ($result as $cart):
                            ?>
                            <tr>
                                <td>
                                    <img src="http://localhost/project/ControlPanel/Assets/uploads/products/<?= $cart['picture'] ?>"
                                         alt="" class="img-responsive"></td>
                                <td>
                                    <p class="product_name"><?= $cart['product_title'] ?></p>
                                    <p class="product_details">Variant: 16 GB, Color: Black,</p>
                                    <p class="product_details"><?= $cart['unite_price'] ?> tk</p>
                                </td>
                                <td>
                                    <form method="post" action="Views/Cart/update.php">
                                        <input id="select" class="" step="1" min="0" max="" name="quantity"
                                               value="<?= $cart['qty'] ?>" title="Qty" size="4"
                                               pattern="[0-9]*" type="number">
                                        <input type="text" hidden value="<?= $cart['id']?>" name="id">
                                        <input type="text" hidden value="<?= $cart['unite_price']?>" name="unit_price">
                                        <button type="submit">Update</button>
                                    </form>
                                </td>
                                <td><p><?= $cart['total_price'] ?> tk</p></td>
                                <td><a href="Views/Cart/delete.php?id=<?= $cart['id']?>"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        <?php
                        endforeach;
                    }else {


                        ?>
                        <tr>
                            <td colspan="5">You have no product on your cart.</td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
            <div class="col-md-4">
                <div class="sidebar">
                    <table class="table">
                        <tr>
                            <td>
                                <p class="lfloat">1 item </p>
                                <p class="rfloat"> BDT. 95.00 tk</p><br> <br>
                                <p class="lfloat">Sipping</p>
                                <p class="rfloat">BDT. 0.00tk</p>
                            </td>
                        </tr>
                        <tr>
                            <td>

                                <p class="lfloat">Total (tax excl.)</p>
                                <p class="rfloat"> BDT. 102.00 tk.</p> <br> <br>
                                <p class="lfloat">Taxes</p>
                                <p class="rfloat">BDT. 0.00tk</p>
                        </tr>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-warning">Proceed to checkout</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <p><i class="glyphicon glyphicon-ok"></i> We provide best quality hand-picked products</p>
                <p><i class="glyphicon glyphicon-ok"></i> We provide best quality hand-picked products</p>
                <p><i class="glyphicon glyphicon-ok"></i> We provide best quality hand-picked products</p>
            </div>
        </div>
    </div>
</div>


<?php
echo $appconfig->element('sponser.php');
echo $appconfig->element('footer.php');
echo $appconfig->element('js.php');

?>
