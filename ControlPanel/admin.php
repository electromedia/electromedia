<?php
session_start();
if($_SESSION['user']!=true){
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}
//var_dump($_SESSION['user']);
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
use rashed\Utility\AppConfig;

$appconfig = new AppConfig();

echo $appconfig->backElement('head.php');

?>


    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <?= $appconfig->backElement('nav.php'); ?>
            </nav>

            <!--        content body-->
            <div class="content_body">

            </div>
        </div>
    </div>
<?php
echo $appconfig->backElement('js.php');
?>