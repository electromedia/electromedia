<?php

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
use rashed\Utility\AppConfig;

$appconfig = new AppConfig();

echo $appconfig->backElement('head.php');
?>
<style>
    body {
        margin: 0;
        padding: 0;
        background: url(Assets/images/sign_back.jpg);
        background-size: cover;
        font-family: 'Roboto Slab', serif;
        background-attachment: fixed;
        background-repeat: no-repeat;
    }
</style>
<div id="sign">
    <div class="container-fluid">
        <div class="loginBox">
            <img class="user" src="http://localhost/project/ControlPanel/Assets/images/logo/signin.png">
            <h2>Log In Here</h2>
            <form method="post" action="signin_action.php">
                <p>Email</p>
                <input type="email" name="email" placeholder="Enter Email" required>
                <p>Password</p>
                <input type="password" name="password" placeholder="Enter Password" required>
               <div class="text-center">
                   <button type="submit">Sign In</button>
               </div>
                <a href="#">Forget Password?</a>
            </form>
        </div>
    </div>
</div>

<?php
echo $appconfig->backElement('js.php');
?>
