<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 8/3/2018
 * Time: 10:16 PM
 */

namespace rashed\Utility;


class AppConfig
{
    const FRONT_ELEMENTS_PATH = '/project/Front/Views/Elements/';

    const HOST='localhost';
    const DB='electro-media';
    const USER='root';
    const PASSWORD='';
    public function element($fileName)
    {
        //return file_get_contents($_SERVER['DOCUMENT_ROOT'] . self::FRONT_ELEMENTS_PATH . $fileName);
        include_once($_SERVER['DOCUMENT_ROOT'] . self::FRONT_ELEMENTS_PATH . $fileName);
    }

    const BACK_ELEMENTS_PATH = '/project/ControlPanel/Views/Elements/';
    public function backElement($name){
        //return file_get_contents($_SERVER['DOCUMENT_ROOT'].self::BACK_ELEMENTS_PATH.$name);
        include_once($_SERVER['DOCUMENT_ROOT'] . self::BACK_ELEMENTS_PATH . $name);
    }
}