<?php

namespace rashed\Utility;

use \PDO;
use rashed\Db\Dal;

class Cart extends Dal
{
    public function listIndex($user=null)
    {
        if (!$user){
            return null;
        }
        $query = "SELECT * FROM `carts` WHERE `sId`= :sId";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam("sId",$user);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function listShow(){
        $query = "SELECT * FROM `carts` WHERE id=".$_GET['id'];
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function store(){
        $product_id=$_POST['product_id'];
        $qty=$_POST['quantity'];
        $unit_price=$_POST['unit_price'];
        $total_price=$_POST['unit_price'] * $qty;
        $picture=$_POST['picture'];
        $product_name=$_POST['product_name'];
        $sid=$_POST['guestId'];

        $query = "INSERT INTO `carts` (`id`,`sId`, `product_id`, `picture`,`product_title`, `qty`, `unite_price`, `total_price`) VALUES (NULL,'$sid', '$product_id', '$picture','$product_name', '$qty', '$unit_price', '$total_price');";
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();
        if ($result){
            header("location:../../shoppingcart.php");
        }else{
            header("location:../../index.php");
        }


    }
    public function update(){

        $qty=$_POST['quantity'];
        $total_price=$_POST['unit_price'] * $qty;

        $query = "UPDATE `carts` SET `qty` = '$qty',`total_price`='$total_price' WHERE `carts`.`id` =".$_POST['id'];
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();
        if ($result){
            header("location:../../shoppingcart.php");
        }

    }
    public function cartDelete(){
        $query = "DELETE FROM `carts` WHERE `carts`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_GET['id']);
        $result = $stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is Permanently Deleted Successfully.");
        } else {
            $message->set("Data is not Deleted.");
        }
    }

}