<?php
/**
 * Created by PhpStorm.
 * User: Rashed
 * Date: 9/16/2018
 * Time: 9:02 PM
 */

namespace rashed\Utility;


class Validator
{
    public static function validateEmpty($var){
        if (empty($var)){
            return true;
        }else{
            return false;
        }
    }
    public static function validateAlpha($var){
     //$pattern="/[[:alpha:]]/";
     $pattern="/^([a-zA-Z]+[\'-]?[a-zA-Z]+[ ]?)+$/";
     return preg_match($pattern,$var);
    }
}