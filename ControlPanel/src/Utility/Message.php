<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 8/8/2018
 * Time: 11:35 AM
 */

namespace rashed\Utility;


class Message
{
    public  function set($message){
        $_SESSION['message'] = $message;
        return true;
    }

    public function get(){
        if(array_key_exists('message',$_SESSION )
            && !empty($_SESSION['message'])){
            $message = $_SESSION['message'];
            $_SESSION['message'] = "";
            return $message;
        }else{
            return null;
        }
    }

    public function has()
    {
        if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
            return true;
        } else {
            return false;
        }

    }
}