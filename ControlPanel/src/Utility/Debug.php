<?php
/**
 * Created by PhpStorm.
 * User: Rashed
 * Date: 9/13/2018
 * Time: 9:20 PM
 */

namespace rashed\Utility;


class Debug
{
    public static function dd($var){
        echo "<pre>";
        print_r($var);
        echo "</pre>";
        die();
    }
}