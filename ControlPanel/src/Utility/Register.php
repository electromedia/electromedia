<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 8/6/2018
 * Time: 10:05 PM
 */

namespace rashed\Utility;

use \PDO;
use rashed\Db\Dal;
use rashed\Utility\Message;


class Register extends Dal
{

    public function store()
    {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $phone = $_POST['phone'];
        $created_at = date("Y-m-d h:i:s", time());
        $modified_at = date("Y-m-d h:i:s", time());

        //$hashedPassword=password_hash($password,PASSWORD_BCRYPT);
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $query = "INSERT INTO `admins` (`id`, `name`, `email`, `password`, `phone`, `created_at`, `modified_at`) VALUES (NULL, '$name', '$email', '$hashedPassword', '$phone', '$created_at', '$modified_at')";
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is inserted Successfully.");
            header("location:../../Views/Register/index.php");
        } else {
            $message->set("Data is not inserted.");
        }
    }

    public function update(){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $phone = $_POST['phone'];
        $modified_at = date("Y-m-d h:i:s", time());

        $query="UPDATE `admins` SET `name` = '$name',`email`='$email',`password`='$password',`phone`='$phone',`modified_at`='$modified_at' WHERE `admins`.`id` =" .$_POST['id'];
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is updated Successfully.");
            header("location:../../Views/Register/index.php");
        } else {
            $message->set("Data is not updated.");
        }
    }

}