<?php
/**
 * Created by PhpStorm.
 * User: Rashed
 * Date: 8/24/2018
 * Time: 6:32 PM
 */

namespace rashed\Utility;


use rashed\Db\Dal;
use rashed\Utility\Message;

class Brands extends Dal
{
    public function store()
    {
        $title = $_POST['title'];
        $link = $_POST['link'];
        $is_draft = $_POST['button'];
        $created_at = date("Y-m-d h:i:s", time());
        $modified_at = date("Y-m-d h:i:s", time());

        $query = "INSERT INTO `brands` (`id`, `title`, `link`, `is_draft`, `created_at`, `modified_at`) VALUES (NULL, '$title', '$link', '$is_draft','$created_at', '$modified_at')";
        $stmt=$this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is inserted SuccessFully.");
            header("location:../../Views/Brands/index.php");
        } else {
            $message->set("Data is not Inserted.");
            header("location:../../Views/Brands/index.php");
        }
    }

    public function update()
    {
        $title = $_POST['title'];
        $link = $_POST['link'];
        $modified_at = date("Y-m-d h:i:s", time());

        $query = "UPDATE `brands` SET `title` = '$title', `link` = '$link', `modified_at` = '$modified_at' WHERE `brands`.`id` =".$_POST['id'];
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is Updated SuccessFully.");
            header("location:../../Views/Brands/index.php");
        } else {
            $message->set("Data is not Updated.");
            header("location:../../Views/Brands/index.php");
        }
    }

    public function frontView(){
        $query = "SELECT * FROM `brands` WHERE `soft_delete`=0 AND `is_draft`=0 ORDER BY `created_at` ASC ";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}