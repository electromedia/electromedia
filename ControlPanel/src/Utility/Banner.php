<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 8/5/2018
 * Time: 10:44 PM
 */

namespace rashed\Utility;

use \PDO;
use rashed\Utility\Message;
use rashed\Utility\Debug;
use rashed\Db\Dal;

class Banner extends Dal
{
//    public function store($data)
//    {
//        $title=$data['title'];
//        $picture =$data['picture']= $_FILES['picture']['name'];
//        $link =$data['link']= $_POST['link'];
//        $promotional_message =$data['promotional_message']= $_POST['promotional_message'];
//        $html_banner =$data['title']= $_POST['html_banner'];
//        $is_active =$data['is_active']= (array_key_exists('is_active', $_POST)) ? $_POST['is_active'] : 0;
//        $is_draft =$data['button']= $_POST['button'];
//        $created_at = date('Y-m-d h:i:s', time());
//        $modified_at =date('Y-m-d h:i:s', time());
//
//        $query = "INSERT INTO `banners` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`,`is_draft`, `created_at`, `modified_at`) VALUES (NULL, '$title', '$picture', '$link', '$promotional_message', '$html_banner', '$is_active','$is_draft', '$created_at', '$modified_at')";
//        $stmt = $this->dbh->prepare($query);
//
//        $result = $stmt->execute();
//
//        $message = new Message();
//        if ($result) {
//            $message->set("Data is inserted successfully.");
//            header("location:../../Views/Banners/index.php");
//        } else {
//            $message->set("Data is not inserted.");
//            header("location:../../Views/Banners/index.php");
//        }
//
//    }
    public function store($data)
    {
        $created_at = date('Y-m-d h:i:s', time());
        $modified_at = date('Y-m-d h:i:s', time());
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "INSERT INTO `banners` (`title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`,`is_draft`, `created_at`, `modified_at`) VALUES (:title, :picture, :link, :promotional_message, :html_banner, :is_active,:is_draft, :created_at, :modified_at)";
        try {
            $stmt = $this->dbh->prepare($query);
            $stmt->bindParam('title', $data['title']);
            $stmt->bindParam('picture', $data['picture']);
            $stmt->bindParam('link', $data['link']);
            $stmt->bindParam('promotional_message', $data['promotional_message']);
            $stmt->bindParam('html_banner', $data['html_banner']);
            $stmt->bindParam('is_active', $data['is_active']);
            $stmt->bindParam('is_draft', $data['button']);
            $stmt->bindParam('created_at', $created_at);
            $stmt->bindParam('modified_at', $modified_at);

            $result = $stmt->execute();
            $message = new Message();
            if ($result) {
                $message->set("Data is inserted successfully.");
                header("location:../../Views/Banners/index.php");
            } else {
                $message->set("Data is not inserted.");
                header("location:../../Views/Banners/index.php");
            }
        } catch (PDOException $e) {
            echo "An exception has occurred,Please try again Later.";
        }
    }


    public function update()
    {

        $title = $_POST['title'];
        $picture = $_FILES['picture']['name'];
        $link = $_POST['link'];
        $promotional_message = $_POST['promotional_message'];
        $html_banner = $_POST['html_banner'];
        $is_active = $_POST['is_active'];

        $query = "UPDATE `banners` SET `title` = '$title', `picture` = '$picture', `link` = '$link', `promotional_message` = '$promotional_message', `html_banner` = '$html_banner', `is_active` = '$is_active' WHERE `banners`.`id` =:id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_POST['id']);
        $result = $stmt->execute();


        $message = new Message();
        if ($result) {
            $message->set("Data is Updated Successfully.");
            header("location:../../Views/Banners/index.php");
        } else {
            $message->set("Data is not Updated.");
            header("location:../../Views/Banners/index.php");
        }
    }

    public function frontView()
    {
        $query = "SELECT * FROM `banners` WHERE `is_active`=1 ORDER BY  `created_at` DESC LIMIT 0,3";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $banners = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $banners;
    }

    public function listIndex($name, $options = [])
    {
        $query = "SELECT * FROM `$name` WHERE " . $this->buildClause($options);
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    private function buildClause($options)
    {
        $clause = 1;
        $clause .= " AND `soft_delete` = 0 AND `is_draft`=0";
        if (array_key_exists('searchTitle', $options)) {
            $clause .= " AND `title` LIKE '%" . $options['searchTitle'] . "%'";
        }
        if (array_key_exists('searchLink', $options)) {
            $clause .= " " . $options['andor1'] . " `link` LIKE '%" . $options['searchLink'] . "%'";
        }
        //process sorting
        if (array_key_exists('sortBy', $options) && $options['sortBy'] == 'titleDesc') {
            $clause .= " ORDER BY `title` DESC";
        } elseif (array_key_exists('sortBy', $options) && $options['sortBy'] == 'titleAsc') {
            $clause .= " ORDER BY `title` ASC";
        }

        //$clause.=" ORDER BY `title` ASC";

        return $clause;
    }


}