<?php
/**
 * Created by PhpStorm.
 * User: Rashed
 * Date: 9/1/2018
 * Time: 3:37 PM
 */

namespace rashed\Utility;


use rashed\Db\Dal;

class Products extends Dal
{
    public function store()
    {
        $title = $_POST['title'];
        $shortDescription = $_POST['shortdescription'];
        $description = $_POST['description'];
        $cost = $_POST['cost'];
        $mrp = $_POST['mrp'];
        $is_draft = $_POST['button'];
        $is_active = $_POST['is_active'];
        $product_type = $_POST['product_type'];
        $specialPrice = $_POST['specialprice'];
        $picture = $_FILES['picture']['name'];
        $created_at = date("Y-m-d h:i:s", time());
        $modified_at = date("Y-m-d h:i:s", time());

        $brand_id = $_POST['brand_id'];
        $lebel_id = $_POST['lebel_id'];
        $total_seles = $_POST['total_seles'];
        $is_new = $_POST['is_new'];


        $query = "INSERT INTO `products` (`id`, `brand_id`, `lebel_id`, `title`, `picture`, `short_description`, `description`, `total_sales`,`product_type`, `is_new`, `cost`, `mrp`, `special_price`,`is_active`,`is_draft`, `created_at`, `modified_at`) VALUES (NULL, '$brand_id', '$lebel_id', '$title', '$picture', '$shortDescription', '$description', '$total_seles','$product_type', '$is_new', '$cost', '$mrp', '$specialPrice','$is_active','$is_draft', '$created_at', '$modified_at')";
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();


        $message = new Message();
        if ($result) {
            $message->set("Product inserted Successfully.");
            header("location:../../Views/Products/index.php");
        } else {
            $message->set("Product is not Inserted Successfully.");
            header("location:../../Views/Products/index.php");
        }
    }

    public function update()
    {
        $title = $_POST['title'];
        $shortDescription = $_POST['shortdescription'];
        $description = $_POST['description'];
        $cost = $_POST['cost'];
        $mrp = $_POST['mrp'];
        $specialPrice = $_POST['specialprice'];
        $product_type = $_POST['product_type'];
        $picture = $_FILES['picture']['name'];
        $modified_at = date("Y-m-d h:i:s", time());


        $query = "UPDATE `products` SET `title` = '$title', `picture` = '$picture', `short_description` = '$shortDescription', `description` = '$description', `cost` = '$cost', `mrp` = '$mrp', `special_price` = '$specialPrice',`product_type`='$product_type',`modified_at`='$modified_at' WHERE `products`.`id` = " . $_POST['id'];
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Product Update Successfully.");
            header("location:../../Views/Products/index.php");
        } else {
            $message->set("Product is Not Updated");
            header("location:../../Views/Products/index.php");
        }
    }

    public function frontView($type)
    {
        $query = "SELECT * FROM `products` WHERE `is_active`=1 AND `product_type` LIKE '$type' ORDER BY  `created_at` DESC";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function search($options = [])
    {
        $options[] = "";
        $query = "SELECT * FROM `products` WHERE" . $this->buildClause($options);
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }

    private function buildClause($options)
    {
        $clause = " `product_type` LIKE 'New Products'";
        if (array_key_exists('searchKeyword', $_POST)) {
            $clause = '';
            $clause .= " (`product_type` LIKE 'New Products' OR `product_type` LIKE 'Featured Products') AND `title` LIKE '%" . $options['searchKeyword'] . "%' OR `short_description` LIKE '%" . $options['searchKeyword'] . "%' OR `description` LIKE '%" . $options['searchKeyword'] . "%'";
        }
        return $clause;
    }

    public function description()
    {
        $query = "SELECT * FROM `products` WHERE `id`= :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id',$_GET['id']);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }
    public function downloadExcel(){
        $query="SELECT * FROM `products` WHERE `is_active`=1 AND `is_draft`=0";
        $stmt=$this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


}