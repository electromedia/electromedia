<?php
/**
 * Created by PhpStorm.
 * User: Rashed
 * Date: 9/2/2018
 * Time: 12:01 PM
 */

namespace rashed\Utility;


use rashed\Db\Dal;

class Contact extends Dal
{
    public function store()
    {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $subject = $_POST['subject'];
        $comment = $_POST['message'];
        $date = date("Y-m-d h:i:s", time());

        $query = "INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `comment`, `date`) VALUES (NULL, '$name', '$email', '$subject', '$comment', '$date')";
        $stmt = $this->dbh->prepare($query);
        $result = $stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Message Sent Successfully.");
            header("location:http://localhost/project/Front/contact.php");
        } else {
            $message->set("Message Not Sent.");
            header("location:http://localhost/project/Front/contact.php");
        }
    }

    public function indexMessage()
    {
        $query = "SELECT * FROM `contact` WHERE `status`=0 AND `soft_delete`=0 ORDER BY `date` DESC ";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function seenMessage()
    {
        $query = "SELECT * FROM `contact` WHERE `status`=1 ORDER BY `date` DESC ";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function seen()
    {
        $query = "UPDATE `contact` SET `status` = 1 WHERE `contact`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_GET['id']);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $message = new Message();
        if ($result) {
            $message->set("Set Message as Seen.");
            header("location:../../Views/Contact/index.php");
        } else {
            $message->set("Message set as Seen Unsuccessful.");
            header("location:../../Views/Contact/index.php");
        }
    }

    public function unseen()
    {
        $query = "UPDATE `contact` SET `status` = 0 WHERE `contact`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_GET['id']);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $message = new Message();
        if ($result) {
            $message->set("Set Message as unSeen.");
            header("location:../../Views/Contact/index.php");
        } else {
            $message->set("Message set as unSeen Unsuccessful.");
            header("location:../../Views/Contact/index.php");
        }
    }

    public function messageTrashed()
    {
        $query = "SELECT * FROM `contact` WHERE `soft_delete`=1 ORDER BY `date` DESC ";
        $stmt = $this->dbh->prepare($query);
        return $stmt->execute();
    }

    public function messageRestore()
    {
        $query = "UPDATE `contact` SET `soft_delete`=0 WHERE `contact`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_GET['id']);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        $message = new Message();
        if ($result) {
            $message->set("Message is Restore Successfully");
        } else {
            echo "Message is not restored.";
        }

    }


}