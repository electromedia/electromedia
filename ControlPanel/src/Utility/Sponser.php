<?php
/**
 * Created by PhpStorm.
 * User: Jamshed Ahammed
 * Date: 8/17/2018
 * Time: 10:47 PM
 */

namespace rashed\Utility;


use rashed\Db\Dal;
use rashed\Utility\Message;

class Sponser extends Dal
{
    public function store()
    {
        $title = $_POST['title'];
        $picture = $_FILES['picture']['name'];
        $link = $_POST['link'];
        $promotional_message = $_POST['promotional_message'];
        $html_banner = $_POST['html_banner'];
        $is_active=$_POST['is_active'];
        $is_draft=$_POST['button'];
        $created_at = date("Y-m-d h:i:s", time());
        $modified_at = date("Y-m-d h:i:s", time());


        $query = "INSERT INTO `sponsers` (`id`, `title`, `picture`, `link`,`promotional_message`, `html_banner`,`is_active`,`is_draft`, `created_at`, `modified_at`) VALUES (NULL, '$title', '$picture', '$link','$promotional_message','$html_banner','$is_active','$is_draft', '$created_at', '$modified_at')";
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is inserted Successfully.");
            header("location:../../Views/Sponser/index.php");
        } else {
            echo "Data is not Inserted.";
        }
    }
    public function update()
    {

        $title = $_POST['title'];
        $picture = $_FILES['picture']['name'];
        $link = $_POST['link'];
        $promotional_message = $_POST['promotional_message'];
        $html_banner = $_POST['html_banner'];
        $is_active = $_POST['is_active'];

        $query = "UPDATE `sponsers` SET `title` = '$title', `picture` = '$picture', `link` = '$link', `promotional_message` = '$promotional_message', `html_banner` = '$html_banner', `is_active` = '$is_active' WHERE `sponsers`.`id` =" . $_POST['id'];

        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is Updated Successfully.");
            header("location:../../Views/Sponser/index.php");
        } else {
            $message->set("Data is not Updated.");
        }
    }


    public function frontView()
    {
        $query = "SELECT * FROM `sponsers` WHERE `is_active`=1 ORDER BY  `created_at` DESC LIMIT 0,3";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


}