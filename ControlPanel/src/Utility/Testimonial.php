<?php

namespace rashed\Utility;


use rashed\Db\Dal;
use rashed\Utility\Message;

class Testimonial extends Dal
{
    public function store()
    {
        $picture = $_FILES['picture']['name'];
        $body = $_POST['body'];
        $name = $_POST['name'];
        $designation = $_POST['designation'];
        $is_active = $_POST['is_active'];
        $is_draft = $_POST['button'];
        $soft_delete = $_POST['soft_delete'];
        $created_at = date("Y-m-d h:i:s", time());
        $modified_at = date("Y-m-d h:i:s", time());


        $query = "INSERT INTO `testimonial` (`id`, `picture`, `body`, `name`, `designation`, `is_active`, `is_draft`, `soft_delete`, `created_at`, `modified_at`) VALUES (NULL, '$picture', '$body', '$name', '$designation', '$is_active', '$is_draft', '$soft_delete', '$created_at', '$modified_at')";
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is Inserted Successfully.");
            header("location:../../Views/Testimonial/index.php");
        } else {
            $message->set("Data is not inserted.");
            header("location:../../Views/Testimonial/index.php");
        }
    }

    public function update()
    {
        $picture = $_FILES['picture']['name'];
        $body = $_POST['body'];
        $name = $_POST['name'];
        $designation = $_POST['designation'];
        $is_active = $_POST['is_active'];
        $modified_at = date("Y-m-d h:i:s", time());


        $query = "UPDATE `testimonial` SET `name` = '$name', `picture` = '$picture', `body` = '$body', `designation` = '$designation', `is_active` = '$is_active',`modified_at`='$modified_at' WHERE `testimonial`.`id` =" . $_POST['id'];
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is Updated Successfully.");
            header("location:../../Views/Testimonial/index.php");
        } else {
            $message->set("Data is not Updated.");
            header("location:../../Views/Testimonial/index.php");
        }
    }

    public function frontView()
    {
        $query = "SELECT * FROM `testimonial` WHERE `is_active`=1 AND `soft_delete`=0 ORDER BY  `created_at` DESC LIMIT 0,3";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);

        $testimonials = array();
        foreach ($result as $row) {
            $testimonials[] = $row;
        };
        return $testimonials;
    }
}