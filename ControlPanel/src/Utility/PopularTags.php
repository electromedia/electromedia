<?php
/**
 * Created by PhpStorm.
 * User: Rashed
 * Date: 8/24/2018
 * Time: 7:59 PM
 */

namespace rashed\Utility;


use rashed\Db\Dal;

class PopularTags extends Dal
{
    public function store(){
        $name=$_POST['name'];
        $link=$_POST['link'];
        $is_draft=$_POST['button'];
        $created_at=date("Y-m-d h:i:s",time());
        $modified_at=date("Y-m-d h:i:s",time());

        $query="INSERT INTO `popular_tag` (`id`, `name`, `link`, `is_draft`, `created_at`, `modified_at`) VALUES (NULL, '$name', '$link', '$is_draft', '$created_at', '$modified_at');";
        $stmt=$this->dbh->prepare($query);
        $result=$stmt->execute();

        $message=new Message();
        if ($result){
            $message->set("Data is Inserted Successfully.");
            header("location:../../Views/PopularTag/index.php");
        }else{
            $message->set("Data is not Inserted Successfully.");
            header("location:../../Views/PopularTag/index.php");
        }
    }

    public function update()
    {
        $name = $_POST['name'];
        $link = $_POST['link'];
        $modified_at = date("Y-m-d h:i:s", time());

        $query = "UPDATE `popular_tag` SET `name` = '$name', `link` = '$link', `modified_at` = '$modified_at' WHERE `popular_tag`.`id` =".$_POST['id'];
        $stmt = $this->dbh->prepare($query);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is Updated SuccessFully.");
            header("location:../../Views/PopularTag/index.php");
        } else {
            $message->set("Data is not Updated.");
            header("location:../../Views/PopularTag/index.php");
        }
    }

    public function frontView(){
        $query = "SELECT * FROM `popular_tag` WHERE `soft_delete`=0 AND `is_draft`=0 ORDER BY `created_at` ASC ";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}