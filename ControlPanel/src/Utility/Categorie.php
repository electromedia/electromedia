<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 8/9/2018
 * Time: 10:14 PM
 */

namespace rashed\Utility;

use \PDO;
use rashed\Utility\Message;
use rashed\Db\Dal;

class Categorie extends Dal
{
    public function store()
    {
        $name = $_POST['name'];
        $link = $_POST['link'];
        $is_draft = $_POST['button'];
        $created_at = date("Y-m-d h:i:s", time());
        $modified_at = date("Y-m-d h:i:s", time());
        $query = "INSERT INTO `categories` (`id`, `name`, `link`,`is_draft`, `created_at`, `modified_at`) VALUES (NULL, '$name', '$link', '$is_draft','$created_at', '$modified_at')";
        $stmt = $this->dbh->prepare($query);
        $result = $stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Dat is inserted Successfully");
            header("location:../../Views/Categories/index.php");
        } else {
            echo "Data is not inserted";
        }
    }

    public function update()
    {
        $name = $_POST['name'];
        $link = $_POST['link'];
        $modified_at = date("Y-m-d h:i:s", time());

        $query = "UPDATE `categories` SET `name` = '$name', `link` = '$link',`modified_at`='$modified_at' WHERE `categories`.`id`=" . $_POST['id'];
        $stmt = $this->dbh->prepare($query);
        $result = $stmt->execute();
        $message = new Message();
        if ($result) {
            $message->set("Data is updated successfully");
            header("location:../../Views/Categories/index.php");
        } else {
            echo "Data is not updated";
        }

    }

    public function frontCategory()
    {
        $query = "SELECT * FROM `categories` WHERE `soft_delete`=0 AND `is_draft`=0 ORDER BY `id` ASC ";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}