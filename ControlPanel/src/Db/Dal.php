<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 8/10/2018
 * Time: 4:11 PM
 */

namespace rashed\Db;

use rashed\Utility\Message;
use rashed\Utility\AppConfig;
use \PDO;

class Dal
{
    protected $dbh = null;

    public function __construct()
    {
        try {
            $this->dbh = new PDO("mysql:host=" . AppConfig::HOST . ";dbname=" . AppConfig::DB, AppConfig::USER, AppConfig::PASSWORD);
        } catch (\PDOException $e) {
            echo "Unknown Issue is Detected,Please Try Again Letter";
        }

    }

    public function index($name)
    {
        $query = "SELECT * FROM `$name` WHERE `soft_delete`=0 AND `is_draft` = 0 ORDER BY `created_at` DESC ";
        //$query = "SELECT * FROM `$name` ORDER BY `id` ASC ";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }

    public function show($name)
    {

        $query = "SELECT * FROM `$name` WHERE `id`= :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_GET['id']);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function softDelete($name)
    {
        $query = "UPDATE `$name` SET `soft_delete`=1 WHERE `$name`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_GET['id']);
        $result = $stmt->execute();
        $message = new Message();
        if ($result) {
            $message->set("Data is Deleted Successfully");
        } else {
            $message->set("Data is Not Deleted.");
        }

    }

    public function delete($name)
    {
        $query = "DELETE FROM `$name` WHERE `$name`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_GET['id']);
        $result = $stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is Permanently Deleted Successfully.");
        } else {
            $message->set("Data is not Deleted.");
        }
    }

    public function trashed($name)
    {
        $query = "SELECT * FROM `$name` WHERE `soft_delete`=1 ORDER BY `created_at` DESC ";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function restore($name)
    {
        $query = "UPDATE `$name` SET `soft_delete`=0,`is_active`=1 WHERE `$name`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_GET['id']);
        $result = $stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Data is Restore Successfully");
        } else {
            $message->set("Data is Not Restored");
        }

    }

    public function item_active($name)
    {
        $modified_at = date('Y-m-d h:i:s', time());
        $query = "UPDATE `$name` SET `is_active` = 1,`modified_at`= '$modified_at' WHERE `$name`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id', $_GET['id']);
        $result = $stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Item Activate successfully");
        } else {
            $message->set("Item is not Activate");
        }
    }

    public function item_deactive($name)
    {
        $modified_at = date('Y-m-d h:i:s', time());
        $query = "UPDATE `$name` SET `is_active` = 0,`modified_at`= '$modified_at' WHERE `$name`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id',$_GET['id']);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Item Deactivate Successfully");
        } else {
            $message->set("Item is not Deactivate");
        }
    }

    public function draft($name)
    {
        $query = "SELECT * FROM `$name` WHERE `is_draft`=1 ORDER BY  `created_at` DESC";
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function draftpublish($name)
    {
        $query = "UPDATE `$name` SET `is_draft` = '0' WHERE `$name`.`id` = :id";
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam('id',$_GET['id']);
        $result=$stmt->execute();

        $message = new Message();
        if ($result) {
            $message->set("Item Published Successfully.");
        } else {
            echo "Item is not Published.";
        }
    }
}