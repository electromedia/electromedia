-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2018 at 02:10 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `electro-media`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `phone`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(1, 'Rashed alam', 'rashedctg1719@gmail.com', '494+616+1', '01852346841', 0, 0, '2018-08-14 04:21:37', '2018-08-16 04:09:38'),
(2, 'Rabbi Ahammed', 'rabbi@gmail.com', '45651', '8485', 0, 0, '2018-08-14 04:22:25', '2018-08-14 04:22:25'),
(3, 'Mohi Uddin', 'mohi@gmail.com', 'ras0061769', '98456', 0, 0, '2018-08-20 01:53:22', '2018-08-20 01:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `promotional_message` varchar(255) NOT NULL,
  `html_banner` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `max_display` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`, `is_draft`, `soft_delete`, `max_display`, `created_at`, `modified_at`) VALUES
(1, 'slider1', 'slider1.jpg', 'a.com', 'This is promotional Message 1', 'This is html banner1', 1, 0, 0, 0, '2018-08-14 04:09:14', '2018-08-18 08:48:08'),
(2, 'slider2', 'slider2.jpg', 'b.com', 'this is message2', '', 1, 0, 0, 0, '2018-08-14 04:10:18', '2018-08-21 03:09:41'),
(3, 'Slider3', 'slider3.jpg', 'a.com', 'This is promotional message', 'This is html Banner', 1, 0, 0, 0, '2018-08-23 05:05:52', '2018-08-23 05:05:52');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `link`, `is_draft`, `is_active`, `soft_delete`, `created_at`, `modified_at`) VALUES
(8, 'Hp', 'index.php', 0, 0, 0, '2018-08-24 03:38:28', '2018-08-24 03:38:28'),
(9, 'Asus', 'index.php', 0, 0, 0, '2018-08-24 03:38:52', '2018-08-24 03:38:52'),
(10, 'Dell', 'index.php', 0, 0, 0, '2018-08-24 03:39:18', '2018-08-24 03:39:18'),
(11, 'Lenovo', 'index.php', 0, 0, 0, '2018-08-24 03:39:31', '2018-08-24 03:39:31'),
(12, 'Microsoft', 'index.php', 0, 0, 0, '2018-08-24 03:39:42', '2018-08-24 03:39:42'),
(13, 'Apple', 'index.php', 0, 0, 0, '2018-08-24 03:39:54', '2018-08-24 03:39:54'),
(14, 'HTC', 'index.php', 0, 0, 0, '2018-08-24 03:40:07', '2018-08-24 03:40:07'),
(15, 'Xiao mi', 'index.php', 0, 0, 0, '2018-08-24 03:40:43', '2018-08-24 03:40:43'),
(16, 'Acer', 'index.php', 0, 0, 0, '2018-08-24 03:41:09', '2018-08-24 03:41:09'),
(17, 'Samsung', 'index.php', 0, 0, 0, '2018-08-24 03:41:25', '2018-08-24 03:41:25'),
(18, 'Walton', 'index.php', 0, 0, 0, '2018-08-24 03:41:34', '2018-08-24 03:41:34');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `link`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(2, 'Desktop accessories', 'index.php', 0, 0, '2018-08-14 04:39:01', '2018-08-16 03:27:29'),
(3, 'Laptop', 'index.php', 0, 0, '2018-08-14 04:39:12', '2018-08-14 04:39:12'),
(4, 'Mobile', 'index.php', 0, 0, '2018-08-14 04:39:21', '2018-08-14 04:39:21'),
(5, 'Head phone and Accessories', 'index.php', 0, 0, '2018-08-17 04:53:58', '2018-08-17 04:53:58'),
(10, '', '', 0, 1, '2018-08-21 03:23:25', '2018-08-21 03:23:25');

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `map_product_tag`
--

CREATE TABLE `map_product_tag` (
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `popular_tag`
--

CREATE TABLE `popular_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `popular_tag`
--

INSERT INTO `popular_tag` (`id`, `name`, `link`, `is_active`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(1, 'Headphones', 'index.php', 1, 0, 0, '0000-00-00 00:00:00', '2018-08-24 04:27:19'),
(5, '', '', 0, 1, 0, '2018-08-24 04:27:31', '2018-08-24 04:27:31'),
(6, 'New', 'index.php', 0, 0, 0, '2018-08-24 04:28:02', '2018-08-24 04:28:02'),
(7, 'Laptop', 'index.php', 0, 0, 0, '2018-08-24 04:28:15', '2018-08-24 04:28:15'),
(8, 'Best Sell', 'index.php', 0, 0, 0, '2018-08-24 04:28:26', '2018-08-24 04:28:26'),
(9, 'Popular Products', 'index.php', 0, 0, 0, '2018-08-24 04:28:45', '2018-08-24 04:28:45'),
(10, 'Rated', 'index.php', 0, 0, 0, '2018-08-24 04:28:53', '2018-08-24 04:28:53'),
(11, 'Desktop', 'index.php', 0, 0, 0, '2018-08-24 04:29:08', '2018-08-24 04:29:08'),
(12, 'Tv', 'index.php', 0, 0, 0, '2018-08-24 04:29:16', '2018-08-24 04:29:16');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `lebel_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `short_description` text,
  `description` text,
  `total_sales` int(11) DEFAULT '0',
  `is_new` tinyint(4) DEFAULT NULL,
  `cost` float NOT NULL,
  `mrp` float NOT NULL,
  `special_price` float NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `brand_id`, `lebel_id`, `title`, `picture`, `short_description`, `description`, `total_sales`, `is_new`, `cost`, `mrp`, `special_price`, `soft_delete`, `is_draft`, `is_active`, `created_at`, `modified_at`) VALUES
(2, 0, 0, 'computer', 'c1.jpg', 'shortDescription', 'description', 0, 0, 0, 0, 0, 0, 0, 0, '2018-09-01 11:56:20', '2018-09-01 02:01:36'),
(3, 0, 0, 'headphone', 'bt.jpg', 'fjsflksmf', 'fnsfns', 0, 0, 300.2, 350, 320, 0, 0, 1, '2018-09-01 11:58:04', '2018-09-01 11:58:04'),
(67, 0, 0, 'dkflksd', '', '', '', 0, 0, 0, 0, 0, 1, 0, 0, '2018-09-01 01:53:19', '2018-09-01 01:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `sponsers`
--

CREATE TABLE `sponsers` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `promotional_message` varchar(255) NOT NULL,
  `html_banner` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsers`
--

INSERT INTO `sponsers` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`, `is_draft`, `soft_delete`, `created_at`, `modified_at`) VALUES
(9, 'dfsdf', 'img_2.png', 'sdfsdf', 'ssdfsd', '', 1, 0, 0, '0000-00-00 00:00:00', '2018-08-20 01:54:16'),
(14, 'dfgdfg', 'img_1.png', 'dfgdf', 'dfgdfg', 'ghf', 1, 0, 0, '2018-08-18 04:53:30', '2018-08-21 03:24:04');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_subscribed` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `picture`, `body`, `name`, `designation`, `is_active`, `is_draft`, `soft_delete`, `created_at`, `modified_at`) VALUES
(2, 't1.png', 'Hello', 'Rashed Alam', 'web Developer', 1, 0, 1, '2018-08-23 07:46:22', '2018-08-23 08:27:00'),
(3, 't2.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, similique.', 'Rabbi Ahammed', 'Web Developer', 1, 0, 0, '2018-08-23 08:31:45', '2018-08-23 03:53:52'),
(5, 't1.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, similique.', 'Rashed Alam', 'Web Developer', 1, 0, 0, '2018-08-23 09:00:14', '2018-08-23 03:53:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popular_tag`
--
ALTER TABLE `popular_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsers`
--
ALTER TABLE `sponsers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `popular_tag`
--
ALTER TABLE `popular_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `sponsers`
--
ALTER TABLE `sponsers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
