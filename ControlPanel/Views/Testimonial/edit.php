<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Banner;
use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
$testimonials = new Banner();
$result = $testimonials->show('testimonial');
foreach ($result as $testimonial) {

};
echo $appconfig->backElement('head.php');
?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner_new">
                <div class="container">
                    <h2 class="text-center">Update Testimonial</h2>
                    <form class="form-group" action="update.php" method="post" enctype="multipart/form-data">

                        <input type="text" hidden name="id" value="<?= $testimonial['id'] ?>">

                        <label for="title">Name :</label>
                        <input type="text" class="form-control" name="name" value="<?= $testimonial['name'] ?>" id="title"
                               placeholder="Enter Your Name.">

                        <label for="designation">Designation :</label>
                        <input type="text" class="form-control" id="designation" value="<?= $testimonial['designation'] ?>" name="designation"
                               placeholder="Enter your designation.">

                        <label for="description">Description :</label>
                        <textarea type="text" class="form-control" name="body"
                                  value="<?= $testimonial['description'] ?>"
                                  id="description" placeholder="Enter Your description."></textarea>

                        <input type="radio" name="is_active" id="active">Is active

                        <input type="file" name="picture" value="<?= $testimonial['picture'] ?>"><br>

                        <div class="text-center">
                            <button type="submit" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php')
?>









