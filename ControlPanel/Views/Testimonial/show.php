<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Testimonial;
use rashed\Utility\AppConfig;

$testimonials = new Testimonial();
$result = $testimonials->show('testimonial');
$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');


?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <dl>
                        <?php
                        foreach ($result as $testimonial) {
                            ?>
                            <dt>Name</dt>
                            <dd><?= $testimonial['name'] ?></dd>
                            <dt>Picture</dt>
                            <dd>
                                <img src="http://localhost/project/ControlPanel/Assets/uploads/testimonial/<?php echo $testimonial['picture'] ?>"
                                     height="auto" width="100%"></dd>
                            <dt>Description</dt>
                            <dd><?= $testimonial['body'] ?></dd>
                            <dt>Designation</dt>
                            <dd><?= $testimonial['designation'] ?></dd>
                            <dt>Status</dt>
                            <dd><?= $testimonial['is_active'] ?></dd>
                            <?php
                        }
                        ?>
                    </dl>

                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








