<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if(array_key_exists('user',$_SESSION)&& $_SESSION['user']!=true){
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');

?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php') ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner_new">
                <div class="container">
                    <h2 class="text-center">Add New Testimonial</h2>
                    <form class="form-group" action="store.php" method="post" enctype="multipart/form-data">
                        <label for="name">Name :</label>
                        <input type="text" class="form-control" name="name" id="name"
                               placeholder="Enter Your Name.">

                        <label for="designation">Designation :</label>
                        <input type="text" class="form-control" id="designation" name="designation" placeholder="Enter your Designation.">


                        <label for="description">Description :</label>
                        <textarea type="text" class="form-control" name="body" id="description"
                                  placeholder="Enter Banner message."></textarea>

                        <input type="radio" name="is_active" id="active" value="1">Is active

                        <input type="file" name="picture"><br>

                        <div class="text-center">

                            <button type="submit" name="button" href="draft.php" value="1">Save as Draft</button>
                            <button type="submit" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/project/ControlPanel/vendor/autoload.php";
echo $appconfig->backElement('js.php');
?>

