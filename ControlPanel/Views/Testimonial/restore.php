<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();

use rashed\Utility\Testimonial;
use rashed\Utility\Message;

$testimonial = new Testimonial();
$testimonial->restore('testimonial');

header("location:../../Views/Testimonial/index.php");
?>