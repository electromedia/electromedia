<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if(array_key_exists('user',$_SESSION)&& $_SESSION['user']!=true){
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\Testimonial;
use rashed\Utility\Message;
use rashed\Utility\AppConfig;

$result = new Testimonial();
$testimonials = $result->index('testimonial');
$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');

?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner">
                <div class="container">
                    <?php
                    $message = new Message();
                    if ($message->has()):
                        ?>
                        <div class="alert alert-success">
                            <?php echo $message->get(); ?>
                        </div>
                    <?php
                    endif;
                    ?>
                    <div class="offset-10">
                        <a href="create.php"><i class="fa fa-plus-circle"></i>NEW</a>
                    </div>
                    <table class="table table-bordered text-center table-dark table-hover">
                        <tr>
                            <th>Name</th>
                            <th>Picture</th>
                            <th>Designation</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        foreach ($testimonials as $testimonial):
                            ?>
                            <tr>
                                <?php
                                    $id=$testimonial['id']
                                ?>

                                <td><?= $testimonial['name'] ?></td>
                                <td>
                                    <img src="http://localhost/project/ControlPanel/Assets/uploads/testimonial/<?php echo $testimonial['picture'] ?>"
                                         height="100px" width="200px" class="img-responsive"></td>
                                <td><?= $testimonial['designation'] ?></td>
                                <td>
                                    <?php
                                    if ($testimonial['is_active']) {
                                        echo '<a href="deactive.php?id=' . $id . '">Deactivate</a>';
                                    } else {
                                        echo '<a href="active.php?id=' . $id . '">Activate</a>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <ul>
                                        <li><a href="edit.php?id=<?= $testimonial['id'] ?>"><i
                                                        class="fas fa-edit"></i></a></li>
                                        <li><a href="show.php?id=<?= $testimonial['id'] ?>"><i
                                                        class="fas fa-eye"></i></a></li>
                                        <li><a href="soft_delete.php?id=<?= $testimonial['id'] ?>">

                                                <i class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td>

                            </tr>
                        <?php
                        endforeach;
                        ?>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
echo $appconfig->backElement('js.php');

?>

