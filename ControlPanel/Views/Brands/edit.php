<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();

use rashed\Utility\Brands;
use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
$brands = new Brands();
$result = $brands->show('brands');
foreach ($result as $brand) {

};

echo $appconfig->backElement('head.php');


?>


    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <?php echo $appconfig->backElement('nav.php'); ?>
            </nav>

            <!--        content body-->
            <div class="content_body">
                <section id="banner_new">
                    <div class="container">
                        <form class="form-group" action="update.php" method="post">
                            <input type="text" hidden name="id" value="<?= $brand['id'] ?>">
                            <label for="name">Brand Title :</label>
                            <input type="text" id="name" value="<?= $brand['title'] ?>" placeholder="Enter your Brand Title"
                                   name="title" class="form-control">

                            <label for="link">Link :</label>
                            <input type="text" id="link" value="<?= $brand['link'] ?>" placeholder="Enter your link"
                                   name="link" class="form-control">


                            <div class="text-center">
                                <button type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>

<?php echo $appconfig->backElement('js.php'); ?>