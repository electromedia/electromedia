<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if(array_key_exists('user',$_SESSION)&& $_SESSION['user']!=true){
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\AppConfig;
use rashed\Utility\Brands;
use rashed\Utility\Message;

$brands = new Brands();
$result = $brands->index('brands');

$appconfig = new AppConfig();

echo $appconfig->backElement("head.php");
?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--content body-->
        <div class="content_body">
            <section id="banner">
                <?php
                $message = new Message();
                if ($message->has()): ?>
                    <div class="alert alert-success">
                        <?php echo $message->get(); ?>
                    </div>
                <?php
                endif;
                ?>
                <div class="container">
                    <div class="offset-10">
                        <a href="create.php"><i class="fa fa-plus-circle"></i>NEW</a>
                    </div>
                    <table class="table table-bordered text-center table-dark table-hover">
                        <tr>
                            <td>Name</td>
                            <td>Link</td>
                            <td>Action</td>
                        </tr>
                        <?php
                        foreach ($result as $brand):
                            ?>
                            <tr>
                                <td><?= $brand['title'] ?></td>
                                <td><?= $brand['link'] ?></td>
                                <td>
                                    <ul>
                                        <li><a href="edit.php?id=<?= $brand['id'] ?>"><i
                                                    class="fas fa-edit"></i></a></li>
                                        <li><a href="show.php?id=<?= $brand['id'] ?>"><i
                                                    class="fas fa-eye"></i></a></li>
                                        <li><a href="soft_delete.php?id=<?= $brand['id'] ?>"><i
                                                    class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>

<?php
echo $appconfig->backElement('js.php');
?>
