<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Brands;
use rashed\Utility\AppConfig;
$row = new Brands();
$brands = $row->show('brands');

$appconfig=new AppConfig();
echo $appconfig->backElement('head.php');

?>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <?php echo $appconfig->backElement('sidebar_and_logo.php');?>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <?php echo $appconfig->backElement('nav.php');?>
            </nav>

            <!--        content body-->
            <div class="content_body">
                <section>
                    <div class="container">
                        <table class="table table-bordered text-center">
                            <tr>
                                <th>Id</th>
                                <th>Brand Title</th>
                                <th>Link</th>
                            </tr>

                            <?php
                            foreach ($brands as $brand) :
                                ?>
                                <tr>
                                    <td><?= $brand['id'] ?></td>
                                    <td><?= $brand['title'] ?></td>
                                    <td><?= $brand['link'] ?></td>
                                </tr>
                            <?php
                            endforeach;
                            ?>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
<?php echo $appconfig->backElement('js.php');?>