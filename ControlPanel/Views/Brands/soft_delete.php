<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();

use rashed\Utility\Brands;
use rashed\Utility\Message;

$brands = new Brands();
$brands->softDelete('brands');

header("location:../../Views/Brands/index.php");
?>