<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();

use rashed\Utility\Contact;
use rashed\Utility\Message;
use rashed\Utility\AppConfig;

$appconfig = new AppConfig();

$contacts = new Contact();
$result = $contacts->messageTrashed();

echo $appconfig->backElement('head.php');
?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <?php
                    $message = new Message();
                    if ($message->has()):
                        ?>
                        <div class="alert alert-success">
                            <?php echo $message->get(); ?>
                        </div>
                    <?php
                    endif;
                    ?>
                    <table class="table table-bordered text-center">
                        <tr>
                            <td>Serial No.</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Action</td>
                        </tr>

                        <?php
                        foreach ($result as $contact):
                            ?>
                            <tr>
                                <?php
                                $id = $contact['id'];
                                ?>
                                <td><?php echo $contact['id'] ?></td>
                                <td><?php echo $contact['name'] ?></td>
                                <td><?php echo $contact['email'] ?></td>
                                <td>
                                    <ul style="list-style-type: none">
                                        <li style="display: inline-block"><a href="restore.php?id=<?= $contact['id'] ?>"><i
                                                        class="fas fa-redo"></i></a></li>
                                        <li style="display: inline-block"><a
                                                    href="delete.php?id=<?= $contact['id'] ?>">
                                                <i class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td>


                            </tr>
                        <?php
                        endforeach;
                        ?>

                    </table>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








