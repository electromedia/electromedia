<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Contact;
use rashed\Utility\AppConfig;

$result = new Contact();
$contacts = $result->show('contact');
$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');

foreach ($contacts as $contact){};


?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <form>
                        <div class="row">
                            <!--name-->
                            <div class="col-md-12">
                                <label for="name">Name</label>
                                <input name="name" id="name" type="text" readonly value="<?= $contact['name']?>" class="form-control">
                            </div>

                            <!--email-->
                            <div class="col-md-12">
                                <label for="email">Email</label>
                                <input name="email" id="email" type="text" readonly value="<?= $contact['email']?>" class="form-control">
                            </div>

                            <!--comment here-->
                            <div class="col-md-12">
                                <label for="subject">Subject</label>
                                <input name="subject" id="subject" value="<?= $contact['subject']?>" type="text" class="form-control" readonly>

                                <label for="message">Massage</label>
                                <input id="message" readonly value="<?= $contact['comment']?>" type="text" class="form-control">
                            </div>
                        </div>

                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








