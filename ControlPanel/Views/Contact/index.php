<?php

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if (array_key_exists('user', $_SESSION) && $_SESSION['user'] != true) {
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\AppConfig;
use rashed\Utility\Contact;
use rashed\Utility\Message;

$result = new Contact();
$contacts = $result->indexMessage();

$result=new Contact();
$messages=$result->seenMessage();

$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');


?>
<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--content body-->
        <div class="content_body">
            <section id="banner">
                <h2 class="text-center">Watch New Messages.</h2>
                <div class="container">
                    <?php
                    $message = new Message();
                    if ($message->has()):
                        ?>
                        <div class="alert alert-success">
                            <?php echo $message->get(); ?>
                        </div>
                    <?php
                    endif;
                    ?>
                    <table class="table table-bordered text-center table-dark table-hover">
                        <tr>
                            <td>Serial No.</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Action</td>
                        </tr>
                        <?php
                        foreach ($contacts as $contact) {
                            ?>
                            <tr>
                                <?php
                                $id = $contact['id'];
                                ?>
                                <td><?php echo $contact['id'] ?></td>
                                <td><?php echo $contact['name'] ?></td>
                                <td><?php echo $contact['email'] ?></td>
                                <td>
                                    <ul>
                                        <li><a href="reply.php?id=<?= $contact['id'] ?>"><i
                                                        class="fas fa-edit"></i></a></li>
                                        <li><a href="show.php?id=<?= $contact['id'] ?>"><i
                                                        class="fas fa-eye"></i></a></li>
                                        <li><a href="seen.php?id=<?= $contact['id'] ?>"><i
                                                        class="fas fa-check"></i></a></li>
                                        <li><a href="soft_delete.php?id=<?= $contact['id'] ?>"><i
                                                        class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td>


                            </tr>

                            <?php
                        }
                        ?>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
echo $appconfig->backElement('js.php');
unset($banner);
?>
