<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Contact;
use rashed\Utility\AppConfig;

$result = new Contact();
$contacts = $result->show('contact');
$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');

foreach ($contacts as $contact){};


?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <form action="reply_action.php" method="post">
                        <div class="row">

                            <input type="text" hidden value="<?= $contact['name']?>">
                            <!--To email-->
                            <div class="col-md-12">
                                <label for="emailTo">To</label>
                                <input name="emailTo" id="emailTo" type="text" readonly value="<?= $contact['email']?>" class="form-control">
                            </div>

                            <div class="col-md-12">
                                <label for="email">From</label>
                                <input name="email" id="email" type="text"  placeholder="Enter Your Email" class="form-control">
                            </div>

                            <!--comment here-->
                            <div class="col-md-12">
                                <label for="subject">Subject</label>
                                <input name="subject" id="subject"  type="text" placeholder="Enter Your Subject" class="form-control">

                                <label for="message">Massage</label>
                                <textarea id="message" rows="10" cols="20" name="message" class="form-control" placeholder="Enter Your Reply"></textarea>
                            </div>

                        </div>

                        <div class="text-center">
                            <button type="submit" class="button" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








