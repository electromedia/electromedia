<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Banner;
use rashed\Utility\AppConfig;

$banner = new Banner();
$result = $banner->show('banners');
$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');


?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <dl>
                        <?php
                        foreach ($result as $user) {
                            ?>
                            <dt>Title</dt>
                            <dd><?= $user['title'] ?></dd>
                            <dt>Picture</dt>
                            <dd>
                                <img src="http://localhost/project/ControlPanel/Assets/uploads/<?php echo $user['picture'] ?>"
                                     height="auto" width="100%"></dd>
                            <dt>Link</dt>
                            <dd><?= $user['link'] ?></dd>
                            <dt>Promotional Message</dt>
                            <dd><?= $user['promotional_message'] ?></dd>
                            <dt>HTML Banner</dt>
                            <dd><?= $user['html_banner'] ?></dd>
                            <dt>Active</dt>
                            <dd><?= $user['is_active'] ?></dd>
                            <?php
                        }
                        ?>
                    </dl>

                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








