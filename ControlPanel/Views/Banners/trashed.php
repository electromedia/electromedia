<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();

use rashed\Utility\Banner;
use rashed\Utility\Message;
use rashed\Utility\AppConfig;

$appconfig = new AppConfig();

$banners = new Banner();
$result = $banners->trashed('banners');

echo $appconfig->backElement('head.php');
?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <?php
                    $message = new Message();
                    if ($message->has()):
                        ?>
                        <div class="alert alert-success">
                            <?php echo $message->get(); ?>
                        </div>
                    <?php
                    endif;
                    ?>
                    <table class="table table-bordered text-center">
                        <tr>
                            <td>Title</td>
                            <td>Picture</td>
                            <td>Link</td>
                            <td>Action</td>
                        </tr>

                        <?php
                        foreach ($result as $banner):
                            ?>
                            <tr>
                                <?php
                                $id = $banner['id'];
                                ?>
                                <td><?php echo $banner['title'] ?></td>
                                <td>
                                    <img src="http://localhost/project/ControlPanel/Assets/uploads/<?php echo $banner['picture'] ?>"
                                         height="100" width="100%"></td>
                                <td><?php echo $banner['link'] ?></td>
                                <td>
                                    <ul style="list-style-type: none">
                                        <li style="display: inline-block"><a href="restore.php?id=<?= $banner['id'] ?>"><i
                                                        class="fas fa-redo"></i></a></li>
                                        <li style="display: inline-block"><a
                                                    href="delete.php?id=<?= $banner['id'] ?>">
                                                <i class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td>


                            </tr>
                        <?php
                        endforeach;
                        ?>

                    </table>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
    echo $appconfig->backElement('js.php');
?>








