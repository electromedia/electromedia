<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Banner;
use rashed\Utility\Downloader;
use rashed\Utility\Debug;

$banner=new Banner();
$banners=$banner->listIndex('banners');
$download=new Downloader();

$activeSheet = $spreadsheet->getActiveSheet();
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Title')
    ->setCellValue('B1', 'Link!')
    ->setCellValue('C1', 'Promotional Message')
    ->setCellValue('D1', 'HTML Banner');
$i=2;
foreach ($banners as $banner){
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A'.$i, $banner['title'])
    ->setCellValue('B'.$i, $banner['link'])
    ->setCellValue('C'.$i, $banner['promotional_message'])
    ->setCellValue('D'.$i, $banner['html_banner']);
$i++;
}
$download->downloadExcel();

