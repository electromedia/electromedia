<?php
session_start();

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Banner;
use rashed\Utility\Validator;
use rashed\Utility\Message;
use rashed\Utility\Debug;

$message = new Message();

$data = [];
$picture = $_FILES['picture']['name'];
move_uploaded_file($_FILES['picture']['tmp_name'], "../../Assets/uploads/" . $picture);
$error=false;
if (array_key_exists('title', $_POST)) {
    if (Validator::validateEmpty($_POST['title'])) {
        $message->set("Title is mandatory");
        $error=true;
        header("location:create.php");
    } elseif (!Validator::validateAlpha($_POST['title'])) {
        $message->set("Please Enter a Validate Title");
        $error=true;
        header("location:create.php");
    }
}
//if(//$message->has()) {
if (!$error) {
    $data['title'] = $_POST['title'];
    $data['picture'] = $_FILES['picture']['name'];
    $data['link'] = $_POST['link'];
    $data['promotional_message'] = $_POST['promotional_message'];
    $data['html_banner'] = $_POST['html_banner'];
    $data['is_active'] = (array_key_exists('is_active', $_POST)) ? $_POST['is_active'] : 0;
    $data['button'] = (array_key_exists('button', $_POST)) ? $_POST['button'] : 0;
    $banner = new Banner();
    $banner->store($data);
}
//}