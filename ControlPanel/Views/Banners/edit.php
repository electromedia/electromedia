<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
use rashed\Utility\Banner;
use rashed\Utility\AppConfig;
$appconfig=new AppConfig();
$banner=new Banner();
$result=$banner->show('banners');
foreach ($result as $user) {

};
echo $appconfig->backElement('head.php');
?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php');?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
           <?php echo $appconfig->backElement('nav.php');?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner_new">
                <div class="container">
                    <h2 class="text-center">Add New Slider</h2>
                    <form class="form-group" action="update.php" method="post" enctype="multipart/form-data">

                        <input type="text" hidden name="id" value="<?= $user['id'] ?>">

                        <label for="title">Title :</label>
                        <input type="text" class="form-control" name="title" value="<?= $user['title'] ?>" id="title"
                               placeholder="Enter slider title.">

                        <label for="link">Link :</label>
                        <input type="text" class="form-control" id="link" value="<?= $user['link'] ?>" name="link"
                               placeholder="Enter your link.">

                        <label for="promotional_message">Promotional Message :</label>
                        <input type="text" class="form-control" value="<?= $user['promotional_message'] ?>"
                               name="promotional_message" id="promotional_message"
                               placeholder="Enter promotional message.">

                        <label for="html_banner">HTML Banner :</label>
                        <textarea type="text" class="form-control" name="html_banner" value="<?= $user['html_banner'] ?>"
                                  id="html_banner" placeholder="Enter Banner message."></textarea>

                        <input type="radio" name="is_active" id="active">Is active

                        <input type="file" name="picture" value="<?= $user['picture'] ?>"><br>

                        <div class="text-center">
                            <button type="submit" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
    echo $appconfig->backElement('js.php')
?>









