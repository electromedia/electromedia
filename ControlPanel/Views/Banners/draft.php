<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Banner;
use rashed\Utility\AppConfig;

$banner = new Banner();
$result = $banner->draft('banners');
$appconfig = new AppConfig();


echo $appconfig->backElement('head.php');


?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <table class="table table-bordered text-center">
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Picture</th>
                            <th>Link</th>
                            <th>Promotional Message</th>
                            <th>HTML Banner</th>
                            <th>Action</th>
                        </tr>

                        <?php
                        foreach ($result as $user) {
                            ?>
                            <tr>
                                <?php
                                $id = $user['id'];
                                ?>
                                <td><?= $user['id'] ?></td>
                                <td><?= $user['title'] ?></td>
                                <td>
                                    <img src="http://localhost/project/ControlPanel/Assets/uploads/<?php echo $user['picture'] ?>"
                                         height="100" width="100">
                                </td>
                                <td><?= $user['link'] ?></td>
                                <td><?= $user['promotional_message'] ?></td>
                                <td><?= $user['html_banner'] ?></td>
                                <td>
                                    <div class="text-center button">
                                        <a href="draft_publish.php?id=<?= $id ?>"><i
                                                    class="fa fa-plus-circle"></i>Publish</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }

                        ?>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








