<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();

use rashed\Utility\Banner;
use rashed\Utility\Message;

$banner = new Banner();
$banner->softDelete('banners');

header("location:../../Views/Banners/index.php");
?>