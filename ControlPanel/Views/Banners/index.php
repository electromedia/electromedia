<?php

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if (array_key_exists('user', $_SESSION) && $_SESSION['user'] != true) {
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\AppConfig;
use rashed\Utility\Banner;
use rashed\Utility\Message;

$searchTitle = "";
$searchLink = "";
if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
    $searchTitle = $_POST['searchTitle'];
    $searchLink = $_POST['searchLink'];
//    die();
}
$Banner = new Banner();
$banners = $Banner->listIndex('banners', $_POST);
//var_dump($banners);
//die();
$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');


?>
<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner">
                <div class="container">
                    <?php
                    $message = new Message();
                    if ($message->has()):
                        ?>
                        <div class="alert alert-success">
                            <?php echo $message->get(); ?>
                        </div>
                    <?php
                    endif;
                    ?>


                    <div class="offset-8">
                        <a href="create.php" class="button"><i class="fa fa-plus-circle"></i>NEW</a>
                        <a href="download.php" class="button"><i class="fa fa-plus-circle"></i>Download</a>
                    </div>
                    <form method="post" action="index.php">
                        <table>
                            <tr>
                                <td><label>Title</label>
                                    <input type="text" placeholder="Enter Keywoard" name="searchTitle"
                                           value="<?php echo $searchTitle ?>">
                                </td>
                                <?php
                                if (array_key_exists('andor1', $_POST) && $_POST['andor1'] == 'AND') {
                                    $radioAnd = "checked='checked'";
                                    $radioOr = '';
                                } elseif (array_key_exists('andor1', $_POST) && $_POST['andor1'] == 'OR') {
                                    $radioOr = "checked='checked'";
                                    $radioAnd = '';
                                } else {
                                    $radioAnd = "checked='checked'";
                                    $radioOr = '';
                                }
                                ?>
                                <td>
                                    <input type="radio" name="andor1" value="AND" <?php echo $radioAnd ?>>AND
                                    <input type="radio" name="andor1" value="OR" <?php echo $radioOr ?>>OR
                                </td>
                                <td>
                                    <label>Link</label>
                                    <input type="text" placeholder="Enter Keywoard" name="searchLink"
                                           value="<?php echo $searchLink ?>">
                                </td>
                                <td>&nbsp;</td>
                                <td class="text-center">Sort By
                                    <select name="sortBy" class="orderBy">
                                        <?php

                                        if (array_key_exists('sortBy', $_POST) && $_POST['sortBy'] == 'titleAsc') {
                                            $titleAscSelected = "selected='selected'";
                                            $titleDescSelected = '';
                                        } else if (array_key_exists('sortBy', $_POST) && $_POST['sortBy'] == 'titleDesc') {
                                            $titleDescSelected = "selected='selected'";
                                            $titleAscSelected = '';
                                        } else {
                                            $titleDescSelected = '';
                                            $titleAscSelected = '';
                                        }
                                        ?>
                                        <option value="Default sorting">Select Your Sorting Type</option>
                                        <option value="titleAsc" <?= $titleAscSelected ?>>A-Z</option>
                                        <option value="titleDesc" <?= $titleDescSelected ?>>Z-A</option>
                                    </select>
                                </td>
                                <td>
                                    <button type="submit" class="button_two">Search</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <?php
                    if (count($banners) < 1) {

                        ?>
                        <table class="table table-bordered table-responsive table-striped table-dark">
                            <tr>
                                <td class="">No Record Found</td>
                            </tr>
                        </table>
                        <?php
                    } else {
                        ?>
                        <table class="table table-bordered text-center table-dark table-hover">
                            <tr>
                                <td>Title</td>
                                <td>Picture</td>
                                <td>Link</td>
                                <td>Is active</td>
                                <td>Action</td>
                            </tr>
                            <?php
                            foreach ($banners as $banner) {
                                ?>
                                <tr>
                                    <?php
                                    $id = $banner['id'];
                                    ?>
                                    <td><?php echo $banner['title'] ?></td>
                                    <td>
                                        <img src="http://localhost/project/ControlPanel/Assets/uploads/<?php echo $banner['picture'] ?>"
                                             height="100" width="100%"></td>
                                    <td><?php echo $banner['link'] ?></td>
                                    <td>
                                        <?php
                                        if ($banner['is_active']) {
                                            echo '<a href="deactive.php?id=' . $id . '">Deactivate</a>';
                                        } else {
                                            echo '<a href="active.php?id=' . $id . '">Activate</a>';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <ul>
                                            <li><a href="edit.php?id=<?= $banner['id'] ?>"><i
                                                            class="fas fa-edit"></i></a></li>
                                            <li><a href="show.php?id=<?= $banner['id'] ?>"><i
                                                            class="fas fa-eye"></i></a></li>
                                            <li><a href="soft_delete.php?id=<?= $banner['id'] ?>">

                                                    <i class="fas fa-trash-alt"></i></a></li>
                                        </ul>
                                    </td>


                                </tr>

                                <?php
                            }
                            ?>
                        </table>
                        <?php
                    }
                    ?>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
echo $appconfig->backElement('js.php');
unset($banner);
?>
