<?php

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Cart;
use rashed\Utility\AppConfig;
use rashed\Utility\Message;

$appconfig=new AppConfig();
$cart=new Cart();
$result=$cart->listIndex();

echo $appconfig->backElement('head.php');
?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner">
                <div class="container">

                    <table class="table table-bordered text-center table-dark table-hover">
                        <tr>
                            <th>Product Id</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Total Price</th>
                            <th>Action</th>
                        </tr>
                        <?php
                            foreach ($result as $cart):
                        ?>
                            <tr>
                                <td><?= $cart['product_id']?></td>
                                <td><?= $cart['qty']?></td>
                                <td><?= $cart['unite_price']?></td>
                                <td><?= $cart['total_price']?></td>
                                <td>
                                    <ul>
                                        <li><a href="edit.php?id=<?= $cart['id'] ?>"><i
                                                        class="fas fa-edit"></i></a></li>
                                        <li><a href="show.php?id=<?= $cart['id'] ?>"><i
                                                        class="fas fa-eye"></i></a></li>
                                        <li><a href="delete.php?id=<?= $cart['id'] ?>">

                                                <i class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td>                            </tr>
                        <?php
                            endforeach;
                        ?>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
echo $appconfig->backElement('js.php');
unset($banner);
?>

