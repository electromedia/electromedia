<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
use rashed\Utility\AppConfig;
use rashed\Utility\Message;
use rashed\Utility\Cart;
$cart=new Cart();
$result=$cart->listShow();
foreach ($result as $cart){

};
$message = new Message();

$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');

?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php') ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner_new">
                <div class="container">
                    <?php
                    if ($message->has()) {
                        ?>
                        <div class="alert alert-danger">
                            <?php
                                echo $message->get();
                            ?>
                        </div>
                        <?php
                    }
                    ?>

                    <form class="form-group" action="update.php" method="post" enctype="multipart/form-data">
                        <input type="text" hidden value="<?= $cart['id']?>" name="id">
                        <label for="product_id">Product Id :</label>
                        <input type="text" class="form-control" name="product_id" id="product_id"
                               placeholder="Enter product id." value="<?= $cart['product_id']?>">

                        <label for="qty">Quantity :</label>
                        <input type="text" class="form-control" id="qty" name="quantity" placeholder="Enter your quantity." value="<?= $cart['qty']?>">

                        <label for="unit_price">Unit Price</label>
                        <input type="text" class="form-control" name="unit_price" id="unit_price"
                               placeholder="Enter Your unit price." value="<?= $cart['unite_price']?>">

                        <label for="total_price">Total Price :</label>
                        <input type="text" class="form-control" name="total_price" id="total_price"
                                  placeholder="Enter Total Price." value="<?= $cart['total_price']?>">

                        <input type="file" name="picture"><br>

                        <div class="text-center">
                            <button type="submit" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
echo $appconfig->backElement('js.php');
?>

