<?php

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Cart;
use rashed\Utility\AppConfig;
use rashed\Utility\Message;
$cart=new Cart();
$result=$cart->listShow();

$appconfig=new AppConfig();


echo $appconfig->backElement('head.php');
?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner">
                <div class="container">

                    <table class="table table-bordered text-center table-dark table-hover">
                        <tr>
                            <th>Product Id</th>
                            <th>Picture</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Total Price</th>
                        </tr>
                        <?php
                            foreach ($result as $row):
                        ?>
                            <tr>
                                <td><?= $row['product_id']?></td>
                                <td><img src="../../Assets/uploads/<?= $row['picture']?>" width="200px" height="100px"></td>
                                <td><?= $row['qty']?></td>
                                <td><?= $row['unite_price']?></td>
                                <td><?= $row['total_price']?></td>
                            </tr>
                        <?php
                            endforeach;
                        ?>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
echo $appconfig->backElement('js.php');
unset($banner);
?>

