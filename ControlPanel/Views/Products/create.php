<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if(array_key_exists('user',$_SESSION)&& $_SESSION['user']!=true){
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');

?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php') ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner_new">
                <div class="container">
                    <h2 class="text-center">Add New Products</h2>
                    <form class="form-group" action="store.php" method="post" enctype="multipart/form-data">
                        <label for="title">Title :</label>
                        <input type="text" class="form-control" name="title" id="title"
                               placeholder="Enter Product title.">

                        <label for="shortdescription">Short Description :</label>
                        <input type="text" class="form-control" id="shortdescription" name="shortdescription" placeholder="Enter Short Description.">

                        <label for="description">Description :</label>
                        <input type="text" class="form-control" name="description" id="description"
                               placeholder="Enter Product Description.">

                        <label for="cost">Cost :</label>
                        <input type="text" class="form-control" name="cost" id="cost"
                                  placeholder="Enter Product Buying Cost.">

                        <label for="mrp">MRP :</label>
                        <input type="text" class="form-control" name="mrp" id="mrp"
                               placeholder="Enter Product MRP.">

                        <label for="specialprice">Special Price :</label>
                        <input type="text" class="form-control" name="specialprice" id="specialprice"
                               placeholder="Enter Product Special Price">

                        <label for="featuredproduct">Product Type :</label>
                        <select id="featuredproduct" class="form-control" name="product_type">
                            <option SELECTED>Chose Your Product Type</option>
                            <option value="New Products" name="product_type">New Products</option>
                            <option value="Featured Products" name="product_type">Featured Products</option>
                        </select>

                        <input type="radio" name="is_active" id="active" value="1">Is active

                        <input type="file" name="picture"><br>

                        <div class="text-center">

                            <button type="submit" name="button" href="draft.php" value="1">Save as Draft</button>
                            <button type="submit" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/project/ControlPanel/vendor/autoload.php";
echo $appconfig->backElement('js.php');
?>

