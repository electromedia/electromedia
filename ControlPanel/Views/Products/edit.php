<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();

use rashed\Utility\AppConfig;
use rashed\Utility\Products;

$result = new Products();
$products = $result->show('products');

$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');

foreach ($products as $product) {

}

?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php') ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner_new">
                <div class="container">
                    <h2 class="text-center">Upadte Your Product</h2>
                    <form class="form-group" action="update.php" method="post" enctype="multipart/form-data">
                        <input type="text" hidden name="id" value="<?= $product['id'] ?>">

                        <label for="title">Title :</label>
                        <input type="text" class="form-control" name="title" id="title"
                               value="<?= $product['title'] ?>">

                        <label for="shortdescription">Short Description :</label>
                        <input type="text" class="form-control" id="shortdescription" name="shortdescription"
                               value="<?= $product['short_description'] ?>">

                        <label for="description">Description :</label>
                        <input type="text" class="form-control" name="description" id="description"
                               value="<?= $product['description'] ?>">

                        <label for="cost">Cost :</label>
                        <input type="text" class="form-control" name="cost" id="cost"
                               value="<?= $product['cost'] ?>">

                        <label for="mrp">MRP :</label>
                        <input type="text" class="form-control" name="mrp" id="mrp"
                               value="<?= $product['mrp'] ?>">

                        <label for="specialprice">Special Price :</label>
                        <input type="text" class="form-control" name="specialprice" id="specialprice"
                               value="<?= $product['special_price'] ?>">

                        <label for="featuredproduct">Product Type :</label>
                        <select id="featuredproduct" class="form-control" name="product_type">
                            <option SELECTED>Chose Your Product Type</option>
                            <option value="New Products" name="product_type">New Products</option>
                            <option value="Featured Products" name="product_type">Featured Products</option>
                        </select>

                        <input type="radio" name="is_active" id="active" value="1">Is active

                        <img src="http://localhost/project/ControlPanel/Assets/uploads/products/<?php echo $product['picture'] ?>"
                             width="200px" height="200px">
                        <input type="file" name="picture"><br>

                        <div class="text-center">
                            <button type="submit" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
echo $appconfig->backElement('js.php');
?>

