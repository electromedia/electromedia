<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Products;
use rashed\Utility\AppConfig;

$products = new Products();
$result = $products->draft('products');
$appconfig = new AppConfig();


echo $appconfig->backElement('head.php');


?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <table class="table table-bordered text-center">
                        <tr>
                            <th>Title</th>
                            <th>Picture</th>
                            <th>Short Description</th>
                            <th>MRP</th>
                            <th>Action</th>
                        </tr>

                        <?php
                        foreach ($result as $product) {
                            ?>
                            <tr>
                                <?php
                                $id = $product['id'];
                                ?>
                                <td><?= $product['title'] ?></td>
                                <td>
                                    <img src="http://localhost/project/ControlPanel/Assets/uploads/products/<?php echo $product['picture'] ?>"
                                         height="100" width="100">
                                </td>
                                <td><?= $product['short_description'] ?></td>
                                <td><?= $product['mrp'] ?></td>

                                <td>
                                    <div class="text-center button">
                                        <a href="draft_publish.php?id=<?= $id ?>"><i
                                                class="fa fa-plus-circle"></i>Publish</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }

                        ?>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








