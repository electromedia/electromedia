<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if (array_key_exists('user', $_SESSION) && $_SESSION['user'] != true) {
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\AppConfig;
use rashed\Utility\Message;
use rashed\Utility\Products;

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;

$result = new Products();
$products = $result->index('products');
if (array_key_exists('page', $_GET)) {
    $currentPage = $_GET['page'];
} else {
    $currentPage = 1;
}

if (!array_key_exists('maxDisplay',$_SESSION)){
    $_SESSION['maxDisplay']=3;
}
if (strtolower($_SERVER['REQUEST_METHOD'])=='post') {
    $_SESSION['maxDisplay']=$_POST['maxDisplay'];
}


$adapter = new ArrayAdapter($products);

$pagerfanta = new Pagerfanta($adapter);
$pagerfanta->setMaxPerPage($_SESSION['maxDisplay']); // 10 by default
$maxPerPage = $pagerfanta->getMaxPerPage();
$pagerfanta->setCurrentPage($currentPage); // 1 by default
$currentPage = $pagerfanta->getCurrentPage();
$nbResults = $pagerfanta->getNbResults(); //total records in Data Base
$currentPageResults = $pagerfanta->getCurrentPageResults(); //result bsed on Current page Number and Max per page


$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');

?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--content body-->
        <div class="content_body">
            <div class="row filter">
                <div class="col-md-4">
                    <?php
                    $message = new Message();
                    if ($message->has()):
                        ?>
                        <div class="alert alert-success">
                            <?php echo $message->get(); ?>
                        </div>
                    <?php
                    endif;
                    ?>
                    <div class="pagination">
                        <ul>
                            <?php
                            if ($pagerfanta->hasPreviousPage()) {
                                ?>
                                <li><a href="index.php?page=<?= $pagerfanta->getPreviousPage() ?>"> << </a></li>
                                <?php
                            }
                            ?>
                            <?php
                            $totalPages = ceil($nbResults / $maxPerPage);
                            if ($pagerfanta->haveToPaginate()) {
                                for ($pageNumber = 1; $pageNumber <= $totalPages; $pageNumber++) {
                                    echo "<li><a href='index.php?page=" . $pageNumber . "'>" . $pageNumber . "</a></li>";
                                }
                            } else {
                                echo "<li>No Pagination Required.</li>";
                            }
                            ?>
                            <?php
                            if ($pagerfanta->hasNextPage()) {
                                ?>
                                <li><a href="index.php?page=<?= $pagerfanta->getNextPage() ?>"> >> </a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dropdown inline">
                        <form action="index.php" method="post">
                            <span>Show:</span>

                            <select name="maxDisplay" class="orderby btn dropdown">
                                <?php
                                $selected='';
                                for ($i = 3; $i < 7; $i++):
                                    if ($i==$_SESSION['maxDisplay']){
                                    $selected='selected=selected';
                                    }else{
                                        $selected='';
                                    }
                                    ?>
                                    <option value="<?= $i;?>" <?= $selected?>><?= $i;?></option>
                                <?php
                                endfor;
                                ?>
                            </select>
                            <button type="submit" class="button_two">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <a href="create.php" class="button"><i class="fa fa-plus-circle"></i>NEW</a>
                    <a href="download.php" class="button"><i class="fa fa-plus-circle"></i>Download</a>

                </div>
            </div>
            <section id="banner">
                <div class="container">


                    <table class="table table-bordered text-center table-dark table-hover">
                        <tr>
                            <th>Title</th>
                            <th>Picture</th>
                            <th>Product Type</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        foreach ($currentPageResults as $product):
                            ?>
                            <tr>
                                <?php
                                $id = $product['id'];
                                ?>
                                <td><?= $product['title'] ?></td>
                                <td>
                                    <img src="http://localhost/project/ControlPanel/Assets/uploads/products/<?php echo $product['picture'] ?>"
                                         height="100" width="100%">
                                </td>
                                <td><?= $product['product_type'] ?></td>
                                <td>
                                    <?php
                                    if ($product['is_active']) {
                                        echo '<a href="deactive.php?id=' . $id . '">Deactivate</a>';
                                    } else {
                                        echo '<a href="active.php?id=' . $id . '">activate</a>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <ul>
                                        <li><a href="edit.php?id=<?= $product['id'] ?>"><i
                                                        class="fas fa-edit"></i></a></li>
                                        <li><a href="show.php?id=<?= $product['id'] ?>"><i
                                                        class="fas fa-eye"></i></a></li>
                                        <li><a href="soft_delete.php?id=<?= $product['id'] ?>">

                                                <i class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                    </table>

                </div>

            </section>
        </div>
    </div>


</div>


<?php
echo $appconfig->backElement('js.php');

?>

