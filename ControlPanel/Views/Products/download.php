<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Products;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

$product=new Products();
$product->downloadExcel();
$spreadsheet = new Spreadsheet();  /*----Spreadsheet object-----*/
$Excel_writer = new Xls($spreadsheet);  /*----- Excel (Xls) Object*/

$spreadsheet->setActiveSheetIndex(0);
$activeSheet = $spreadsheet->getActiveSheet();

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Title')
    ->setCellValue('B1', 'Price!')
    ->setCellValue('C1', 'Categories')
    ->setCellValue('D1', 'Brands');

$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A2', 'Mouse')
    ->setCellValue('B2', '250 BDT')
    ->setCellValue('C2', 'Accessories')
    ->setCellValue('D2', 'A4 Tech');


$filename='products_'.time();
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'. $filename .'.xls"'); /*-- $filename is  xsl filename ---*/
header('Cache-Control: max-age=0');
$Excel_writer->save('php://output');