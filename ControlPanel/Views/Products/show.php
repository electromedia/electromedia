<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Products;
use rashed\Utility\AppConfig;

$result = new Products();
$products = $result->show('products');
$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');


?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <dl>
                        <?php
                        foreach ($products as $product) {
                            ?>
                            <dt>Title</dt>
                            <dd><?= $product['title'] ?></dd>
                            <dt>Picture</dt>
                            <dd>
                                <img src="http://localhost/project/ControlPanel/Assets/uploads/products/<?php echo $product['picture'] ?>"
                                     height="200px" width="200px"></dd>
                            <dt>Short Description</dt>
                            <dd><?= $product['short_description'] ?></dd>
                            <dt>Description</dt>
                            <dd><?= $product['description'] ?></dd>
                            <dt>Cost</dt>
                            <dd><?= $product['cost'] ?></dd>
                            <dt>MRP</dt>
                            <dd><?= $product['mrp'] ?></dd>
                            <dt>Special Price</dt>
                            <dd><?= $product['special_price'] ?></dd>
                            <dt>Total Sales</dt>
                            <dd><?= $product['total_sales'] ?></dd>
                            <?php
                        }
                        ?>
                    </dl>

                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








