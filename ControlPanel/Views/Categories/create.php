<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if(array_key_exists('user',$_SESSION)&& $_SESSION['user']!=true){
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\AppConfig;
$appconfig=new AppConfig();
echo $appconfig->backElement('head.php');




?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
       <?php echo $appconfig->backElement('sidebar_and_logo.php');?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php');?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner_new">
                <div class="container">
                    <form class="form-group" action="store.php" method="post">
                        <label for="name">Name :</label>
                        <input type="text" id="name" placeholder="Enter your name" name="name" class="form-control">

                        <label for="link">Link :</label>
                        <input type="text" id="link" placeholder="Enter your email" name="link" class="form-control">


                        <div class="text-center">
                            <button type="submit" name="button" value="1">Save as Draft</button>
                            <button type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<?php echo $appconfig->backElement('js.php');?>