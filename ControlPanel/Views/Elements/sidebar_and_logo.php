<div class="sidebar-header">
    <a href="#"><img src="http://localhost/project/ControlPanel/Assets/images/logo/logo.png" alt=""
                     class="img-fluid"></a>
</div>

<ul class="list-unstyled components">
    <li><a href="#"><i class="fas fa-tachometer-alt"></i>Dashboard</a></li>

    <li>
        <a href="#inbox" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                    class="fas fa-envelope"></i>Inbox</a>
        <ul class="collapse list-unstyled" id="inbox">
            <li><a href="http://localhost/project/ControlPanel/Views/Contact/index.php">New Message</a></li>
            <li><a href="http://localhost/project/ControlPanel/Views/Contact/unseen_inbox.php">Seen Message</a></li>
            <li><a href="http://localhost/project/ControlPanel/Views/Contact/trashed.php">Trashed</a></li>
        </ul>
    </li>

    <li>
        <a href="#categories" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                    class="fas fa-bookmark"></i>Categories</a>
        <ul class="collapse list-unstyled" id="categories">
            <li><a href="http://localhost/project/ControlPanel/Views/Categories/index.php">List</a></li>
            <li><a href="http://localhost/project/ControlPanel/Views/Categories/draft.php">Draft</a></li>
            <li><a href="http://localhost/project/ControlPanel/Views/Categories/trashed.php">Trashed</a></li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fas fa-user-friends"></i>Client</a>
    </li>
    <li>
        <a href="#"><i class="fas fa-book-open"></i>orders</a>
    </li>
    <li>
        <a href="#"><i class="fas fa-american-sign-language-interpreting"></i>Products</a>
    </li>
    <li>
        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                    class="fas fa-cogs"></i>Settings</a>
        <ul class="collapse list-unstyled" id="homeSubmenu">
            <li>
                <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Slider</a>
                <ul class="collapse list-unstyled" id="homeSubmenu2">
                    <li><a href="http://localhost/project/ControlPanel/Views/Banners/index.php">List</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Banners/draft.php">Draft</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Banners/trashed.php">Trashed</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#products" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Products</a>
                <ul class="collapse list-unstyled" id="products">
                    <li><a href="http://localhost/project/ControlPanel/Views/Products/index.php">List</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Products/draft.php">Draft</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Products/trashed.php">Trashed</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#sponser" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Register</a>
                <ul class="collapse list-unstyled" id="sponser">
                    <li><a href="http://localhost/project/ControlPanel/Views/Register/index.php">List</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Register/trashed.php">Trashed</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#cart" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Add cart</a>
                <ul class="collapse list-unstyled" id="cart">
                    <li><a href="http://localhost/project/ControlPanel/Views/Cart/index.php">List</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Cart/trashed.php">Trashed</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#register" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Sponser</a>
                <ul class="collapse list-unstyled" id="register">
                    <li><a href="http://localhost/project/ControlPanel/Views/Sponser/index.php">List</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Sponser/draft.php">Draft</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Sponser/trashed.php">Trashed</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#testimonial" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">Testimonial</a>
                <ul class="collapse list-unstyled" id="testimonial">
                    <li><a href="http://localhost/project/ControlPanel/Views/Testimonial/index.php">List</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Testimonial/draft.php">Draft</a>
                    </li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Testimonial/trashed.php">Trashed</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#brands" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">Brands</a>
                <ul class="collapse list-unstyled" id="brands">
                    <li><a href="http://localhost/project/ControlPanel/Views/Brands/index.php">List</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Brands/draft.php">Draft</a>
                    </li>
                    <li><a href="http://localhost/project/ControlPanel/Views/Brands/trashed.php">Trashed</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#tag" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">Popular tags</a>
                <ul class="collapse list-unstyled" id="tag">
                    <li><a href="http://localhost/project/ControlPanel/Views/PopularTag/index.php">List</a></li>
                    <li><a href="http://localhost/project/ControlPanel/Views/PopularTag/draft.php">Draft</a>
                    </li>
                    <li><a href="http://localhost/project/ControlPanel/Views/PopularTag/trashed.php">Trashed</a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fas fa-clone"></i>About</a>
    </li>
    <li>
        <a href="#"><i class="fas fa-user-alt"></i>Customers</a>
    </li>
    <li>
        <a href="http://localhost/project/ControlPanel/logout.php"><i class="fas fa-sign-out-alt"></i>LogOut</a>
    </li>
</ul>