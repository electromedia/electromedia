<?php
$dbh = new PDO("mysql:host=localhost;dbname=electro-media", "root", "");
$query = "SELECT COUNT(*)as newmessage FROM `contact` WHERE `status`=0 ";
$result = $dbh->query($query);
foreach ($result as $row) {
    $newMessage = $row['newmessage'];
}

?>
<div class="container-fluid">

    <button type="button" id="sidebarCollapse" class="btn btn-info d-md-none">
        <i class="fas fa-align-left"></i>
    </button>
    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <i class="fas fa-align-justify"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="http://localhost/project/ControlPanel/Views/Contact/index.php"><i class="fas fa-envelope"></i>
                    <?php
                    if ($newMessage) {
                        echo "(" . $newMessage . ")";

                    } else {
                        echo(0);
                    }
                    ?>
                </a>
            </li>
        </ul>
    </div>
</div>