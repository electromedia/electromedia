<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\PopularTags;
use rashed\Utility\AppConfig;

$result = new PopularTags();
$tags = $result->draft('popular_tag');
$appconfig = new AppConfig();


echo $appconfig->backElement('head.php');


?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <table class="table table-bordered text-center">
                        <tr>
                            <th>Name</th>
                            <th>Link</th>
                            <th>Action</th>
                        </tr>

                        <?php
                        foreach ($tags as $tag) :
                            ?>
                            <tr>
                                <?php
                                $id = $tag['id'];
                                ?>
                                <td><?= $tag['name'] ?></td>
                                <td><?= $tag['link'] ?></td>
                                <td>
                                    <div class="text-center button">
                                        <a href="draft_publish.php?id=<?= $id ?>"><i
                                                class="fa fa-plus-circle"></i>Publish</a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        endforeach;

                        ?>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








