<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();

use rashed\Utility\PopularTags;
use rashed\Utility\Message;

$tags = new PopularTags();
$tags->softDelete('popular_tag');

header("location:../../Views/PopularTag/index.php");
?>