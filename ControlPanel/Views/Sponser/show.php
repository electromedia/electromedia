<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Sponser;
use rashed\Utility\AppConfig;

$result = new Sponser();
$sponsers = $result->show('sponsers');
$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');


?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section>
                <div class="container">
                    <dl>
                        <?php
                        foreach ($sponsers as $sponser) {
                            ?>
                            <dt>Title</dt>
                            <dd><?= $sponser['title'] ?></dd>
                            <dt>Picture</dt>
                            <dd>
                                <img src="http://localhost/project/ControlPanel/Assets/uploads/sponser<?php echo $sponser['picture'] ?>"
                                     height="" width=""></dd>
                            <dt>Link</dt>
                            <dd><?= $sponser['link'] ?></dd>
                            <dt>Promotional Message</dt>
                            <dd><?= $sponser['promotional_message'] ?></dd>
                            <dt>HTML Banner</dt>
                            <dd><?= $sponser['html_banner'] ?></dd>
                            <dt>Active</dt>
                            <dd><?= $sponser['is_active'] ?></dd>
                            <?php
                        }
                        ?>
                    </dl>

                </div>
            </section>
        </div>
    </div>
</div>
<?php
echo $appconfig->backElement('js.php');
?>








