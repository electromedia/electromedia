<?php

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if(array_key_exists('user',$_SESSION)&& $_SESSION['user']!=true){
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\AppConfig;
use rashed\Utility\Sponser;
use rashed\Utility\Message;


$result = new Sponser();
$sponsers = $result->index('sponsers');
$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');


?>
<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner">
                <div class="container">
                    <?php
                    $message = new Message();
                    if ($message->has()):
                        ?>
                        <div class="alert alert-success">
                            <?php echo $message->get(); ?>
                        </div>
                    <?php
                    endif;
                    ?>
                    <div class="offset-10">
                        <a href="create.php"><i class="fa fa-plus-circle"></i>NEW</a>
                    </div>
                    <table class="table table-bordered text-center table-dark table-hover">
                        <tr>
                            <td>Title</td>
                            <td>Picture</td>
                            <td>Link</td>
                            <td>Is active</td>
                            <td>Action</td>
                        </tr>
                        <?php
                        foreach ($sponsers as $sponser) {
                            ?>
                            <tr>
                                <?php
                                $id = $sponser['id'];
                                ?>
                                <td><?php echo $sponser['title'] ?></td>
                                <td>
                                    <img src="http://localhost/project/ControlPanel/Assets/uploads/sponser<?php echo $sponser['picture'] ?>"
                                         height="100" width="100%"></td>
                                <td><?php echo $sponser['link'] ?></td>
                                <td>
                                    <?php
                                    if ($sponser['is_active']) {
                                        echo '<a href="deactive.php?id=' . $id . '">Deactivate</a>';
                                    } else {
                                        echo '<a href="active.php?id=' . $id . '">Activate</a>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <ul>
                                        <li><a href="edit.php?id=<?= $sponser['id'] ?>"><i
                                                        class="fas fa-edit"></i></a></li>
                                        <li><a href="show.php?id=<?= $sponser['id'] ?>"><i
                                                        class="fas fa-eye"></i></a></li>
                                        <li><a href="soft_delete.php?id=<?= $sponser['id'] ?>"><i
                                                        class="fas fa-trash-alt"></i></a></li>
                                    </ul>
                                </td>


                            </tr>

                            <?php
                        }
                        ?>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
echo $appconfig->backElement('js.php');
unset($sponser);
?>
