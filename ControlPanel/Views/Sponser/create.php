<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if(array_key_exists('user',$_SESSION)&& $_SESSION['user']!=true){
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');

?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php') ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php') ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner_new">
                <div class="container">
                    <h2 class="text-center">Add New Sponser</h2>
                    <form class="form-group" action="store.php" method="post" enctype="multipart/form-data">
                        <label for="title">Title :</label>
                        <input type="text" class="form-control" name="title" id="title"
                               placeholder="Enter slider title.">

                        <label for="link">Link :</label>
                        <input type="text" class="form-control" id="link" name="link" placeholder="Enter your link.">

                        <label for="promotional_message">Promotional Message :</label>
                        <input type="text" class="form-control" name="promotional_message" id="promotional_message"
                               placeholder="Enter promotional message.">

                        <label for="html_banner">HTML Banner :</label>
                        <textarea type="text" class="form-control" name="html_banner" id="html_banner"
                                  placeholder="Enter Banner message."></textarea>

                        <input type="radio" name="is_active" id="active" value="1">Is active

                        <input type="file" name="picture"><br>

                        <div class="text-center">

                            <button type="submit" name="button" href="draft.php" value="1">Save as Draft</button>
                            <button type="submit" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>


<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/project/ControlPanel/vendor/autoload.php";
echo $appconfig->backElement('js.php');
?>

