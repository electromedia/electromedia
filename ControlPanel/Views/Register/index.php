<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
session_start();
if(array_key_exists('user',$_SESSION)&& $_SESSION['user']!=true){
    header("location:http://rashed.alam/project/ControlPanel/signin.php");
}

use rashed\Utility\Register;
use rashed\Utility\Message;
use rashed\Utility\AppConfig;

$All_contact = new Register();
$result = $All_contact->index('admins');

$appconfig = new AppConfig();
echo $appconfig->backElement('head.php');
?>


<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?= $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?= $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner">
                <div class="container">
                    <?php
                    $message = new Message();
                    if ($message->has()): ?>
                        <div class="alert alert-success">
                            <?php
                            echo $message->get();
                            ?>
                        </div>
                    <?php endif; ?>
                    <div class="offset-10">
                        <a href="create.php"><i class="fa fa-plus-circle"></i>NEW</a>
                    </div>
                    <table class="table table-bordered table-dark table-hover text-center">
                        <tr>
                            <td>Id</td>
                            <td>Name</td>
                            <td>Action</td>
                        </tr>

                        <?php
                        foreach ($result as $user) {
                            ?>
                            <tr>
                                <td><?php echo $user['id'] ?></td>
                                <td><?php echo $user['name'] ?></td>
                                <td>
                                    <ul>
                                        <li><a href="show.php?id=<?= $user['id'] ?>"><i
                                                    class="fas fa-eye"></i></li>
                                        <li><a href="edit.php?id=<?= $user['id'] ?>"><i
                                                    class="fas fa-edit"></i></a></li>
                                        <li><a href="soft_delete.php?id=<?= $user['id'] ?>"
                                               onclick="return confirm('Are you sure you want to delete this item')"><i
                                                    class="fas fa-trash-alt"></i></a>
                                        </li>
                                    </ul>
                                </td>


                            </tr>
                            <?php
                        }
                        ?>

                    </table>
                </div>
            </section>
        </div>
    </div>
</div>

<?php
echo $appconfig->backElement('js.php');
unset($banner);
?>


