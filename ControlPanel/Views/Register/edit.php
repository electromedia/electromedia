<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Register;
use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
$All_contact = new Register();
$result = $All_contact->show('admins');


foreach ($result as $user) {

}
echo $appconfig->backElement('head.php');
?>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <?php echo $appconfig->backElement('nav.php'); ?>
        </nav>

        <!--        content body-->
        <div class="content_body">
            <section id="banner_new">
                <div class="container">
                    <form class="form-group" action="update.php" method="post">
                        <input type="text" class="form-control" hidden name="id" value="<?php echo $user['id'] ?>">

                        <label for="name">Name :</label>
                        <input type="text" id="name" name="name" value="<?php echo $user['name'] ?>"
                               class="form-control">

                        <label for="email">Email :</label>
                        <input type="email" id="email" value="<?php echo $user['email'] ?>" name="email"
                               class="form-control">

                        <label for="password">Password :</label>
                        <input type="password" id="password" value="<?php echo $user['password'] ?>" name="password"
                               class="form-control">

                        <label for="phone">phone :</label>
                        <input type="text" id="phone" name="phone" value="<?php echo $user['phone'] ?>"
                               class="form-control">

                        <div class="text-center">
                            <button type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<?php echo $appconfig->backElement('js.php'); ?>
