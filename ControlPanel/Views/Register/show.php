<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "project" . DIRECTORY_SEPARATOR . "ControlPanel" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use rashed\Utility\Register;
use rashed\Utility\AppConfig;

$appconfig = new AppConfig();
$All_contact = new Register();
$result = $All_contact->show('admins');

echo $appconfig->backElement('head.php');


?>

    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <?php echo $appconfig->backElement('sidebar_and_logo.php'); ?>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <?php echo $appconfig->backElement('nav.php'); ?>
            </nav>

            <!--        content body-->
            <div class="content_body">
                <section id="banner">
                    <div class="container">
                        <table class="table table-bordered text-center">
                            <tr>
                                <td>Id</td>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Password</td>
                                <td>Phone</td>
                            </tr>

                            <?php
                            foreach ($result as $user) {
                                ?>
                                <tr>
                                    <td><?php echo $user['id'] ?></td>
                                    <td><?php echo $user['name'] ?></td>
                                    <td><?php echo $user['email'] ?></td>
                                    <td><?php echo $user['password'] ?></td>
                                    <td><?php echo $user['phone'] ?></td>
                                </tr>
                                <?php
                            }
                            ?>

                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>

<?php echo $appconfig->backElement('js.php'); ?>