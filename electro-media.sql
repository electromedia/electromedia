-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2019 at 07:08 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `electro-media`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `phone`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(1, 'Rashed alam', 'rashedctg1719@gmail.com', 'rashed', '01852346841', 0, 0, '2018-08-14 04:21:37', '2018-09-02 07:07:35'),
(2, 'Rabbi Ahammed', 'rabbiahamed728@gmail.com', 'tolon', '8485', 0, 0, '2018-08-14 04:22:25', '2018-08-14 04:22:25'),
(3, 'Mohi Uddin', 'mohi@gmail.com', 'ras0061769', '98456', 0, 0, '2018-08-20 01:53:22', '2018-08-20 01:53:36'),
(4, 'Sajjad', 'sajjad@gmail.com', '$2y$10$SZdlg2KAvxwK0G7MsFKo0e0xEtCl6dJ0TAst/bGDFWY', '541651651651', 1, 0, '2018-09-04 08:04:15', '2018-09-04 08:04:15'),
(5, 'rambo', 'rambo@gmail.com', '$2y$10$POyB/46bibzo9UKWEygg.uzce61u2EpdWFpnlwA4RoO', '415949', 0, 0, '2018-09-04 08:14:04', '2018-09-04 08:14:04'),
(6, '', 'ras@gmail.com', '$2y$10$/Glyr/qDlgh2jA1xx1b7SeSLUWqz.ejhLxoHVTI9B1/', '584', 1, 0, '2018-09-04 07:17:06', '2018-09-04 07:17:06'),
(7, 'rashed', 'rashed@gmail.com', '$2y$10$lfGNpMev9D5hyb3hryOcAuUq3mFK9kuFJ.EcI3Fbhu0', '65165984984', 0, 0, '2018-09-05 07:12:04', '2018-09-05 07:12:04');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `promotional_message` varchar(255) NOT NULL,
  `html_banner` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `max_display` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`, `is_draft`, `soft_delete`, `max_display`, `created_at`, `modified_at`) VALUES
(1, 'slider1', 'slider1.jpg', 'a.com', 'This is promotional Message 1', 'This is html banner1', 1, 0, 0, 0, '2018-08-14 04:09:14', '2018-10-28 01:29:53'),
(2, 'slider2', 'slider2.jpg', 'b.com', 'this is message2', '', 1, 0, 0, 0, '2018-08-14 04:10:18', '2018-10-28 01:29:55'),
(3, 'Slider3', 'slider3.jpg', 'a.com', 'This is promotional message', 'This is html Banner', 1, 0, 0, 0, '2018-08-23 05:05:52', '2018-10-28 01:13:15'),
(21, 'dfsd', '', '', '', '', 0, 1, 0, 0, '2018-09-17 07:56:33', '2018-09-17 07:56:33'),
(22, '', '', '', '', '', 0, 0, 1, 0, '2018-09-17 06:36:38', '2018-09-17 06:36:38'),
(23, 'fsdfs', '35325_20068-UnlimitedColorsPack-050.jpg', 'dfsd', 'dfsdf', 'sdfsd', 0, 0, 1, 0, '2018-09-17 06:39:40', '2018-10-28 01:30:27');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `link`, `is_draft`, `is_active`, `soft_delete`, `created_at`, `modified_at`) VALUES
(8, 'Hp', 'index.php', 0, 0, 0, '2018-08-24 03:38:28', '2018-08-24 03:38:28'),
(9, 'Asus', 'index.php', 0, 0, 0, '2018-08-24 03:38:52', '2018-08-24 03:38:52'),
(10, 'Dell', 'index.php', 0, 0, 0, '2018-08-24 03:39:18', '2018-08-24 03:39:18'),
(11, 'Lenovo', 'index.php', 0, 0, 0, '2018-08-24 03:39:31', '2018-08-24 03:39:31'),
(12, 'Microsoft', 'index.php', 0, 0, 0, '2018-08-24 03:39:42', '2018-08-24 03:39:42'),
(13, 'Apple', 'index.php', 0, 0, 0, '2018-08-24 03:39:54', '2018-08-24 03:39:54'),
(14, 'HTC', 'index.php', 0, 0, 0, '2018-08-24 03:40:07', '2018-08-24 03:40:07'),
(15, 'Xiao mi', 'index.php', 0, 0, 0, '2018-08-24 03:40:43', '2018-08-24 03:40:43'),
(16, 'Acer', 'index.php', 0, 0, 0, '2018-08-24 03:41:09', '2018-08-24 03:41:09'),
(17, 'Samsung', 'index.php', 0, 0, 0, '2018-08-24 03:41:25', '2018-08-24 03:41:25'),
(18, 'Walton', 'index.php', 0, 0, 0, '2018-08-24 03:41:34', '2018-08-24 03:41:34');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `sId` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `unite_price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `sId`, `product_id`, `picture`, `product_title`, `qty`, `unite_price`, `total_price`) VALUES
(29, 29143, 3, 'bt.jpg', 'Audio Technica ATH-M20x Professional Headphone', 1, 350, 350),
(30, 74848, 68, 'c2.jpg', 'HP 15-db0002au AMD Dual Core E2-9000e 15.6', 2, 50000, 100000),
(31, 4190, 3, 'bt.jpg', 'Audio Technica ATH-M20x Professional Headphone', 1, 350, 350),
(32, 84205, 3, 'bt.jpg', 'Audio Technica ATH-M20x Professional Headphone', 2, 350, 700),
(33, 2925, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 2, 55000, 110000),
(34, 49397, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 2, 55000, 110000),
(35, 82335, 68, 'c2.jpg', 'HP 15-db0002au AMD Dual Core E2-9000e 15.6', 3, 50000, 150000),
(36, 2553, 68, 'c2.jpg', 'HP 15-db0002au AMD Dual Core E2-9000e 15.6', 1, 50000, 50000),
(37, 58657, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 4, 55000, 220000),
(39, 9398, 3, 'bt.jpg', 'Audio Technica ATH-M20x Professional Headphone', 3, 350, 1050),
(40, 3377, 3, 'bt.jpg', 'Audio Technica ATH-M20x Professional Headphone', 1, 350, 350),
(41, 57756, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(42, 98931, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 3, 55000, 165000),
(44, 94574, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 2, 55000, 110000),
(46, 28008, 68, 'c2.jpg', 'HP 15-db0002au AMD Dual Core E2-9000e 15.6', 4, 50000, 200000),
(47, 7137, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 3, 55000, 165000),
(48, 23902, 68, 'c2.jpg', 'HP 15-db0002au AMD Dual Core E2-9000e 15.6', 3, 50000, 150000),
(50, 31628, 3, 'bt.jpg', 'Audio Technica ATH-M20x Professional Headphone', 3, 350, 1050),
(51, 2293, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(52, 96950, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 2, 55000, 110000),
(53, 53326, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(54, 66825, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(55, 18311, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(56, 65646, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(57, 8002, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(58, 46264, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(59, 0, 0, '', '', 0, 0, 0),
(60, 14284, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(61, 0, 0, '', '', 0, 0, 0),
(62, 28530, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(63, 20192, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(64, 55181, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(65, 95970, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(66, 53127, 3, 'bt.jpg', 'Audio Technica ATH-M20x Professional Headphone', 0, 350, 0),
(67, 39787, 3, 'bt.jpg', 'Audio Technica ATH-M20x Professional Headphone', 2, 350, 700),
(68, 48750, 69, 'c3.jpg', 'Asus X540YA Amd Dual Core 15.6', 5, 42000, 210000),
(69, 47508, 3, 'bt.jpg', 'Audio Technica ATH-M20x Professional Headphone', 2, 350, 700),
(70, 7724, 74, 'm.jpg', 'Mobile 1', 3, 14000, 42000),
(71, 8955, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000),
(72, 73836, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 3, 55000, 165000),
(73, 31904, 2, 'c1.jpg', 'HP 14-bw077au AMD Dual Core 14', 1, 55000, 55000);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `link`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(2, 'Desktop accessories', 'index.php', 0, 0, '2018-08-14 04:39:01', '2018-08-16 03:27:29'),
(3, 'Laptop', 'index.php', 0, 0, '2018-08-14 04:39:12', '2018-08-14 04:39:12'),
(4, 'Mobile', 'index.php', 1, 0, '2018-08-14 04:39:21', '2018-08-14 04:39:21'),
(5, 'Head phone and Accessories', 'index.php', 1, 0, '2018-08-17 04:53:58', '2018-08-17 04:53:58'),
(10, '', '', 0, 1, '2018-08-21 03:23:25', '2018-08-21 03:23:25');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `soft_delete` int(1) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `comment`, `status`, `soft_delete`, `date`) VALUES
(1, 'dfg', 'dfgd@gmail.com', 'sdfsd', 'dfsdf', 1, 0, '2018-10-28 01:14:25'),
(2, 'dfsdf', 'fsdf@gmail.com', 'dfsd', 'sdfsdf', 1, 0, '2018-10-28 01:15:41'),
(3, 'sdfs', 'fsdf@gmail.com', 'fds', 'sdfs', 0, 0, '2018-10-28 01:20:41');

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `map_product_tag`
--

CREATE TABLE `map_product_tag` (
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `popular_tag`
--

CREATE TABLE `popular_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `popular_tag`
--

INSERT INTO `popular_tag` (`id`, `name`, `link`, `is_active`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(1, 'Headphones', 'index.php', 1, 0, 0, '0000-00-00 00:00:00', '2018-08-24 04:27:19'),
(5, '', '', 0, 1, 0, '2018-08-24 04:27:31', '2018-08-24 04:27:31'),
(6, 'New', 'index.php', 0, 0, 0, '2018-08-24 04:28:02', '2018-08-24 04:28:02'),
(7, 'Laptop', 'index.php', 0, 0, 0, '2018-08-24 04:28:15', '2018-08-24 04:28:15'),
(8, 'Best Sell', 'index.php', 0, 0, 0, '2018-08-24 04:28:26', '2018-08-24 04:28:26'),
(9, 'Popular Products', 'index.php', 0, 0, 0, '2018-08-24 04:28:45', '2018-08-24 04:28:45'),
(10, 'Rated', 'index.php', 0, 0, 0, '2018-08-24 04:28:53', '2018-08-24 04:28:53'),
(11, 'Desktop', 'index.php', 0, 0, 0, '2018-08-24 04:29:08', '2018-08-24 04:29:08'),
(12, 'Tv', 'index.php', 0, 0, 0, '2018-08-24 04:29:16', '2018-08-24 04:29:16');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `lebel_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `short_description` text,
  `description` text,
  `total_sales` int(11) DEFAULT '0',
  `product_type` varchar(255) NOT NULL,
  `is_new` tinyint(4) DEFAULT NULL,
  `cost` float NOT NULL,
  `mrp` float NOT NULL,
  `special_price` float NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `brand_id`, `lebel_id`, `title`, `picture`, `short_description`, `description`, `total_sales`, `product_type`, `is_new`, `cost`, `mrp`, `special_price`, `soft_delete`, `is_draft`, `is_active`, `created_at`, `modified_at`) VALUES
(2, 0, 0, 'HP 14-bw077au AMD Dual Core 14\" HD Laptop', 'c1.jpg', 'HP bw077au 14\" HD laptop with AMD Dual-Core E2-9000e base frequency of 1.5 GHz & 2 GHz burst frequency processor and 4 GB DDR4 RAM is an American stylish brand that also contains 500 GB SATA storage to ensure greater compatibility.', 'HP bw077au 14\" HD laptop with AMD Dual-Core E2-9000e base frequency of 1.5 GHz & 2 GHz burst frequency processor and 4 GB DDR4 RAM is an American stylish brand that also contains 500 GB SATA storage to ensure greater compatibility. In addition, it has AMD Radeon R2 Graphics, Full-size island-style keyboard and TrueVision HD Camera with integrated digital microphone. 4-cell, 41 Wh Li-ion batteries will provide longer power backup on the go and the weightage of only 1.7 kg will ensure high level of portability. This multifunction durable device comes with 01 year of warranty facility.  Find out the best low budget gaming laptop at Star Tech. We ensure quality service and best after sale service in all the major cities such as Chattagram, Dhaka, Rangpur & Sylhet. Now you have luxury to order online to have your desired laptop at your location. ', 0, 'New Products', 0, 50000, 55000, 53000, 0, 0, 1, '2018-09-01 11:56:20', '2018-09-11 01:43:53'),
(3, 0, 0, 'Audio Technica ATH-M20x Professional Headphone', 'bt.jpg', 'fjsflksmf', 'fnsfns', 0, 'New Products', 0, 300.2, 350, 320, 0, 0, 1, '2018-09-01 11:58:04', '2018-10-28 12:57:09'),
(67, 0, 0, 'dkflksd', '', '', '', 0, '0', 0, 0, 0, 0, 1, 0, 0, '2018-09-01 01:53:19', '2018-09-01 01:53:19'),
(68, 0, 0, 'HP 15-db0002au AMD Dual Core E2-9000e 15.6', 'c2.jpg', 'test', 'test', 0, 'New Products', 0, 45000, 50000, 48000, 0, 0, 1, '2018-09-01 04:01:12', '2018-09-11 02:02:19'),
(69, 0, 0, 'Asus X540YA Amd Dual Core 15.6\" Laptop', 'c3.jpg', 'asus', 'asus', 0, 'New Products', 0, 38000, 42000, 40000, 0, 0, 1, '2018-09-01 04:02:00', '2018-09-11 02:02:51'),
(70, 0, 0, 'Dell Inspiron 14-3462 Celeron Dual Core 14', 'c4.jpg', 'Dell', 'Dell', 0, 'New Products', 0, 50000, 55000, 50000, 0, 0, 1, '2018-09-01 04:03:27', '2018-09-11 02:02:35'),
(71, 0, 0, 'Lenovo IP 120s Celeron Dual Core 11.1\" Laptop', 'c6.jpg', 'lanovo', 'lenovo', 0, 'New Products', 0, 45000, 50000, 48000, 0, 0, 1, '2018-09-01 04:06:42', '2018-09-11 02:03:30'),
(72, 0, 0, 'phone', 'm4.png', 'phone', 'phone', 0, 'New Products', 0, 12000, 15000, 14000, 0, 0, 1, '2018-09-01 04:30:03', '2018-09-01 04:30:03'),
(73, 0, 0, '', '', '', '', 0, 'Featured Products', 0, 0, 0, 0, 1, 0, 0, '2018-09-01 04:35:37', '2018-09-01 04:35:37'),
(74, 0, 0, 'Mobile 1', 'm.jpg', 'mobile', 'mobile', 0, 'Featured Products', 0, 12000, 14000, 13000, 0, 0, 1, '2018-09-01 04:56:07', '2018-09-01 04:56:07'),
(75, 0, 0, 'Mobile 2', 'm1.png', 'mobile', 'mobile', 0, 'Featured Products', 0, 8000, 12000, 10000, 0, 0, 1, '2018-09-01 04:56:58', '2018-09-01 04:56:58'),
(76, 0, 0, 'Mobile 3', 'm2.png', 'mobile', 'mobile', 0, 'Featured Products', 0, 10000, 12000, 11500, 0, 0, 1, '2018-09-01 04:57:35', '2018-09-01 04:57:35'),
(77, 0, 0, 'Mobile 4', 'm6.png', 'mobile', 'mobile', 0, 'Featured Products', 0, 5000, 6000, 5000, 0, 0, 1, '2018-09-01 04:58:21', '2018-09-01 04:58:21'),
(78, 0, 0, 'mobile 5', 'm5.png', 'mobile', 'mobile', 0, 'Featured Products', 0, 7000, 8000, 7500, 0, 0, 1, '2018-09-01 05:01:17', '2018-09-01 05:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `sponsers`
--

CREATE TABLE `sponsers` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `promotional_message` varchar(255) NOT NULL,
  `html_banner` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsers`
--

INSERT INTO `sponsers` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`, `is_draft`, `soft_delete`, `created_at`, `modified_at`) VALUES
(9, 'dfsdf', 'img_2.png', 'sdfsdf', 'ssdfsd', '', 1, 0, 0, '0000-00-00 00:00:00', '2018-08-20 01:54:16'),
(14, 'dfgdfg', 'img_1.png', 'dfgdf', 'dfgdfg', 'ghf', 1, 0, 0, '2018-08-18 04:53:30', '2018-08-21 03:24:04');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_subscribed` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `picture`, `body`, `name`, `designation`, `is_active`, `is_draft`, `soft_delete`, `created_at`, `modified_at`) VALUES
(2, 't1.png', 'Hello', 'Rashed Alam', 'web Developer', 1, 0, 1, '2018-08-23 07:46:22', '2018-08-23 08:27:00'),
(3, 't2.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, similique.', 'Rabbi Ahammed', 'Web Developer', 1, 0, 0, '2018-08-23 08:31:45', '2018-09-13 09:06:28'),
(5, 't1.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, similique.', 'Rashed Alam', 'Web Developer', 1, 0, 0, '2018-08-23 09:00:14', '2018-08-23 03:53:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popular_tag`
--
ALTER TABLE `popular_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsers`
--
ALTER TABLE `sponsers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `popular_tag`
--
ALTER TABLE `popular_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `sponsers`
--
ALTER TABLE `sponsers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
